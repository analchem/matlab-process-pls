function [bootstrap, models] = processpls_boot(model, repeats, parallelize, alpha)

%DECRIPTION:
%This function bootstraps a Process PLS model calculated with the function
%'processpls.m'. It will save the Process PLS models for each bootstrap
%repeat. It will also calculate the mean, median, standard deviation and
%confidence limits of all model parameters fitted.
%
%INPUT:
%- model: The to-be bootstrapped Process PLS model as calculated with the
%  function 'processpls.m'.
%- repeats: The number of bootstrap repeats (100, by default).
%- parallelize: Whether or not to parallelize the bootstrapping procedure 
%  over multiple
%  processing cores, if available (false, by default).
%- 'alpha': The confidence level that is used to calculate the two-sided 
%  confidence intervals for the Process PLS model parameters. For an alpha 
%  of 0.05, a 95% confidence interval is calculated.
%
%OUTPUT:

%- bootstrap = The bootstrapped modelling results. For each parameter that 
%  is bootstrapped, the mean, median, standard deviation and confidence 
%  interval is saved. Note that the original model is not included in this 
%  output. The parameters from the original model that are bootstrapped are 
%  the following:
%  - expvar_in_MV_block
%  - path_variances_explaned
%  - nlvs_modelled
%  - variable_contributions
%  - MV_on_other_block
%
%  Additionally, the actual bootstrap samples are saved in this structure
%  as 'bootsamples', for reproducibility reasons. The alpha-value used to
%  calculate the confidence intervals is also saved.
%- models = An array with all the complete Process PLS models for each of
%  the bootstrap samples.
%
%AUTHOR:
%Tim Offermans, Radboud University Nijmegen (The Netherlands), October 2021
%
%VERSION:
%All codes tested on Matlab R2021a.
%
%SYNTAX:
%model = processpls_boot(model, repeats, parallelize)

%Check function input:
if nargin<4
    alpha = 0.05;
end
if nargin<3
    parallelize = false;
end
if nargin<2
    repeats = 100;
end

%Look up Z-value for the alpha-value:
ztable = [0,Inf;0.01,2.576;0.02,2.326;0.03,2.170;0.04,2.054;0.05,1.960;0.06,1.881;0.07,1.812;0.08,1.751;0.09,1.695;0.1,1.645;0.11,1.598;0.12,1.555;0.13,1.514;0.14,1.476;0.15,1.440;0.16,1.405;0.17,1.372;0.18,1.341;0.19,1.311;0.2,1.282;0.21,1.254;0.22,1.227;0.23,1.2;0.24,1.175;0.25,1.150;0.26,1.126;0.27,1.103;0.28,1.080;0.29,1.058;0.3,1.036;0.31,1.015;0.32,0.9940;0.33,0.9740;0.34,0.9540;0.35,0.9350;0.36,0.9150;0.37,0.8960;0.38,0.8780;0.39,0.86;0.4,0.8420;0.41,0.8240;0.42,0.8060;0.43,0.7890;0.44,0.7720;0.45,0.7550;0.46,0.7390;0.47,0.7220;0.48,0.7060;0.49,0.69;0.5,0.6740;0.51,0.6590;0.52,0.6430;0.53,0.6280;0.54,0.6130;0.55,0.5980;0.56,0.5830;0.57,0.5680;0.58,0.5530;0.59,0.5390;0.6,0.5240;0.61,0.51;0.62,0.4960;0.63,0.4820;0.64,0.4680;0.65,0.4540;0.66,0.44;0.67,0.4260;0.68,0.4120;0.69,0.3990;0.7,0.3850;0.71,0.3720;0.72,0.3580;0.73,0.3450;0.74,0.3320;0.75,0.3190;0.76,0.3050;0.77,0.2920;0.78,0.2790;0.79,0.2660;0.8,0.2530;0.81,0.24;0.82,0.2280;0.83,0.2150;0.84,0.2020;0.85,0.1890;0.86,0.1760;0.87,0.1640;0.88,0.1510;0.89,0.1380;0.9,0.1260;0.91,0.1130;0.92,0.1;0.93,0.088;0.94,0.075;0.95,0.063;0.96,0.05;0.97,0.038;0.98,0.025;0.99,0.013;1,0];
alpha = round(alpha, 2);
z = ztable(find(ztable(:, 1)==alpha, 1), 2);

%Prepare bootstrap samples. Note that the bootstrap samples are not sorted
%on original sample index.
bootsamples = ceil(rand(size(model.data, 1), repeats).*size(model.data, 1));
bootstrap = [];
bootstrap.bootsamples  = bootsamples;
bootstrap.alpha = alpha;

%Initialize parallel computing pool:
if parallelize
    %Only open a parallel pool if no one is already running:
    if isempty(gcp('nocreate'))
        %Start as many parallel pools as there are CPU cores, but not more
        %than 12 as this is not supported by Matlab.
        parpool(min([12, feature('numcores')]));
    end
end

%Calculate the Process PLS model for each bootstrap sample and save the
%results in a (large) array of models. A parfor-loop is used in case
%parallelization is requested, otherwise a regular for-loop is used:
models = cell(size(bootsamples, 2), 1);
if parallelize
    parfor b=1:size(bootsamples, 2)
        models{b} = processpls(model.data(bootsamples(:, b), :), model.inner_model_spec, model.outer_model_spec, 'block_names', model.block_names, 'nlvs', model.nlvs);
    end
else
    for b=1:size(bootsamples, 2)
        models{b} = processpls(model.data(bootsamples(:, b), :), model.inner_model_spec, model.outer_model_spec, 'block_names', model.block_names, 'nlvs', model.nlvs);
    end
end

%Calculate bootstrapped values for data_variance_explained:
expvar_in_MV_block = NaN([size(model.expvar_in_MV_block), length(models)]);
for b=1:length(models)
    expvar_in_MV_block(:, :, b) = models{b}.expvar_in_MV_block;
end
bootstrap.expvar_in_MV_block.mean = squeeze(mean(expvar_in_MV_block, 3));
bootstrap.expvar_in_MV_block.median = squeeze(median(expvar_in_MV_block, 3));
bootstrap.expvar_in_MV_block.std = squeeze(std(expvar_in_MV_block, 0, 3));
bootstrap.expvar_in_MV_block.ci_low = bootstrap.expvar_in_MV_block.mean - (z.* (bootstrap.expvar_in_MV_block.std ./ sqrt(length(models))));
bootstrap.expvar_in_MV_block.ci_high = bootstrap.expvar_in_MV_block.mean + (z.* (bootstrap.expvar_in_MV_block.std ./ sqrt(length(models))));

%Calculate bootstrapped values for path_variance_explained:
inner_model = NaN([size(model.inner_model), length(models)]);
for b=1:length(models)
    inner_model(:, :, b) = models{b}.inner_model;
end
bootstrap.inner_model.mean = squeeze(mean(inner_model, 3));
bootstrap.inner_model.median = squeeze(median(inner_model, 3));
bootstrap.inner_model.std = squeeze(std(inner_model, 0, 3));
bootstrap.inner_model.ci_low = bootstrap.inner_model.mean - (z.* (bootstrap.inner_model.std ./ sqrt(length(models))));
bootstrap.inner_model.ci_high = bootstrap.inner_model.mean + (z.* (bootstrap.inner_model.std ./ sqrt(length(models))));

%Calculate bootstrapped values for nlvs_modelled:
nlvs_modelled = NaN([size(model.nlvs_modelled), length(models)]);
for b=1:length(models)
    nlvs_modelled(:, :, b) = models{b}.nlvs_modelled;
end
bootstrap.nlvs_modelled.mean = squeeze(mean(nlvs_modelled, 3));
bootstrap.nlvs_modelled.median = squeeze(median(nlvs_modelled, 3));
bootstrap.nlvs_modelled.std = squeeze(std(nlvs_modelled, 0, 3));
bootstrap.nlvs_modelled.ci_low = bootstrap.nlvs_modelled.mean - (z.* (bootstrap.nlvs_modelled.std ./ sqrt(length(models))));
bootstrap.nlvs_modelled.ci_high = bootstrap.nlvs_modelled.mean + (z.* (bootstrap.nlvs_modelled.std ./ sqrt(length(models))));

%Calculate bootstrapped values for variable_contributions:
variable_contributions = NaN([size(model.variable_contributions), length(models)]);
for b=1:length(models)
    variable_contributions(:, :, b) = models{b}.variable_contributions;
end
bootstrap.variable_contributions.mean = squeeze(mean(variable_contributions, 3));
bootstrap.variable_contributions.median = squeeze(median(variable_contributions, 3));
bootstrap.variable_contributions.std = squeeze(std(variable_contributions, 0, 3));
bootstrap.variable_contributions.ci_low = bootstrap.variable_contributions.mean - (z.* (bootstrap.variable_contributions.std ./ sqrt(length(models))));
bootstrap.variable_contributions.ci_high = bootstrap.variable_contributions.mean + (z.* (bootstrap.variable_contributions.std ./ sqrt(length(models))));

%Calculate bootstrapped values for MV_on_other_block:
MV_on_other_block = NaN([size(model.MV_on_other_block), length(models)]);
for b=1:length(models)
    MV_on_other_block(:, :, b) = models{b}.MV_on_other_block;
end
bootstrap.MV_on_other_block.mean = squeeze(mean(MV_on_other_block, 3));
bootstrap.MV_on_other_block.median = squeeze(median(MV_on_other_block, 3));
bootstrap.MV_on_other_block.std = squeeze(std(MV_on_other_block, 0, 3));
bootstrap.MV_on_other_block.ci_low = bootstrap.MV_on_other_block.mean - (z.* (bootstrap.MV_on_other_block.std ./ sqrt(length(models))));
bootstrap.MV_on_other_block.ci_high = bootstrap.MV_on_other_block.mean + (z.* (bootstrap.MV_on_other_block.std ./ sqrt(length(models))));

end