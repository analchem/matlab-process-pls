function [dataout, prediction] = processpls_predict(model, datain)

%DESCRIPTION:
%This function predicts the values for measured variables from End-node
%blocks in an existing Processs PLS model using new data for measured
%variables from Start- or Middle-node blocks. It essentially allows for the
%use of Process PLS as a regular prediction model, but only for those
%meaured variables that are only response variables in the model and not
%predictors. This is typically the data for the end-block.
%
%In essence, the measured data for all Start- and Middle-blocks are
%projected in the outer PLS models to obtain the X-scores for those blocks.
%Those X-scores are predictors for the inner PLS models that predict the
%Y-scores of the outer PLS models in the End-blocks. Using the outer model
%loadings for that End-block, the measured data for that End-block is then
%calculated. All preprocessing steps are accounted for, using the data in
%the model.
%
%INPUT:
%- model: The Process PLS model to use for this prediction, obtained with
%  the function 'processpls.m'.
%- datain: The new data to use for the predictions. The variables that
%  should be predicted can be empty (NaN) as they are not used for the
%  predictions. However, when available, the function will use them to
%  calculate the model fit in terms of rmse and r2.
%
%OUTPUT:
%- dataout: The predicted data. Note that input-variables will be NaN.
%- prediction: A Matlab-structure that represents the full prediction in 
%  the Process PLS model. It has similar fields as the Process PLS
%  model-structure, although only those fields needed for the prediction
%  are available. The additional fields are:
%  - datain: The new data used for the prediction.
%  - dataout: The predicted data (for the response measured variables in
%    the system).
%  - rmse: The root mean square error for each predicted measured variable, 
%    in case the references were inputted.
%  - r2: The fraction of explained variance for each predicted measured 
%    variable, in case the references were inputted.';
%
%AUTHOR:
%Tim Offermans, Radboud University Nijmegen (The Netherlands), October 2022
%
%VERSION:
%All codes tested on Matlab R2021a.
%
%SYNTAX:
%[dataout, prediction] = processpls_predict(model, datain)

%Initialize prediction-structure:
prediction = [];
prediction.datain = datain;
prediction.dataout = nan(size(prediction.datain));
prediction.block_names = model.block_names;
prediction.inner_model_spec = model.inner_model_spec;
prediction.outer_model_spec = model.outer_model_spec;
prediction.blocks = [];

%Check if the measured variables of the Start- and Middle-nodes contain
%data:
i = any(prediction.inner_model_spec);
i = any(repmat(i, size(prediction.outer_model_spec, 1), 1) .* prediction.outer_model_spec, 2);
i = find(any(isnan(prediction.datain(:, i))));
if ~isempty(i)
    error(['Measured variables [' num2str(i) '] are needed as predictors but contain missing values, please check.']);
end

%Project measured data for Start- and Middle-blocks:
for b=find(any(prediction.inner_model_spec))

    %Copy block name and type:
    prediction.blocks{b, 1}.name = model.blocks{b}.name;
    prediction.blocks{b}.type = model.blocks{b}.type;
        
    %Extract data for this block from input:
    prediction.blocks{b}.X = prediction.datain(:, prediction.outer_model_spec(:, b));
    
    %Repeat the preprocessing for the model data, so that the means,
    %standard deviations and block sum of squares are saved:
    temp = model.blocks{b}.X;
    temp_mean = mean(temp);
    temp = temp - (ones(size(temp, 1), 1) * temp_mean);
    temp_std = std(temp);
    temp = temp ./ (ones(size(temp, 1), 1) * temp_std);
    temp_ss = sqrt(mean((temp(:).^2) * size(temp, 2)));
    
    %Apply the preprocessing of the model data on the new data:
    prediction.blocks{b}.Xpp = prediction.blocks{b}.X;
    prediction.blocks{b}.Xpp = prediction.blocks{b}.Xpp - (ones(size(prediction.blocks{b}.Xpp, 1), 1) * temp_mean);
    prediction.blocks{b}.Xpp = prediction.blocks{b}.Xpp ./ (ones(size(prediction.blocks{b}.Xpp, 1), 1) * temp_std);
    prediction.blocks{b}.Xpp = prediction.blocks{b}.Xpp ./ temp_ss;
        
    %Calculate outer model X-scores:
    prediction.blocks{b}.om_x = prediction.blocks{b}.Xpp;
    prediction.blocks{b}.om_t = prediction.blocks{b}.om_x * model.blocks{b}.om_r;
end

%Predict measured data for End-blocks:
for b=find(~any(prediction.inner_model_spec))
    
    %Copy block name and type, and initialize other fields:
    prediction.blocks{b, 1}.name = model.blocks{b}.name;
    prediction.blocks{b}.type = model.blocks{b}.type;
    prediction.blocks{b}.X = [];
    prediction.blocks{b}.Xpp = [];
    prediction.blocks{b}.om_y = [];
    prediction.blocks{b}.om_u = [];
    prediction.blocks{b}.im_x = [];
    
    %Predict outer model Y-scores:
    prediction.blocks{b}.im_x = [];
    for i=find(prediction.inner_model_spec(b, :))
        prediction.blocks{b}.im_x = [prediction.blocks{b}.im_x, prediction.blocks{i}.om_t];
    end
    prediction.blocks{b}.om_u = model.blocks{b}.im_b(1, :) + (prediction.blocks{b}.im_x * model.blocks{b}.im_b(2:end, :));
    
    %Predict measured data:
    prediction.blocks{b}.om_y = prediction.blocks{b}.om_u * model.blocks{b}.om_q';
    prediction.blocks{b}.om_y = prediction.blocks{b}.om_y ./ sqrt(sum((prediction.blocks{b}.om_y-mean(prediction.blocks{b}.om_y)).^2)/size(prediction.blocks{b}.om_y, 1));
    prediction.blocks{b}.Xpp = prediction.blocks{b}.om_y;
    
    %Repeat the preprocessing for the model data, so that the means,
    %standard deviations and block sum of squares are saved:
    temp = model.blocks{b}.X;
    temp_mean = mean(temp);
    temp = temp - (ones(size(temp, 1), 1) * temp_mean);
    temp_std = std(temp);
    temp = temp ./ (ones(size(temp, 1), 1) * temp_std);
    temp_ss = sqrt(mean((temp(:).^2) * size(temp, 2)));
    temp = temp ./ temp_ss;
    
    %Revert the preprocessing for the predicted data:
    prediction.blocks{b}.X = prediction.blocks{b}.Xpp;
	prediction.blocks{b}.X = prediction.blocks{b}.X .* temp_ss;
    prediction.blocks{b}.X = prediction.blocks{b}.X .* (ones(size(prediction.blocks{b}.X, 1), 1) * temp_std);
    prediction.blocks{b}.X = prediction.blocks{b}.X + (ones(size(prediction.blocks{b}.X, 1), 1) * temp_mean);
    
    %Store the predicted variables in the main data structure:
    prediction.dataout(:, prediction.outer_model_spec(:, b)) = prediction.blocks{b}.X;
end

%Calculate the model fit, in case the reference values are given in the
%input data:
prediction.rmse = NaN(1, size(prediction.datain, 2));
prediction.r2 = NaN(1, size(prediction.datain, 2));
i = ~any(prediction.inner_model_spec);
i = any(repmat(i, size(prediction.outer_model_spec, 1), 1) .* prediction.outer_model_spec, 2);
i = logical(i'.* ~any(isnan(prediction.datain)));
for v=find(i)
    prediction.rmse(v) = sqrt(nanmean((prediction.datain(:, v) - prediction.dataout(:, v)).^2));
    prediction.r2(v) = corr(prediction.datain(:, v), prediction.dataout(:, v), 'rows', 'complete');
end

%Additional output:
prediction.timestamp = datetime();
prediction.info = {
    'datain', 'The inputed measured data for this prediction. End-node data may be inputted for validation purposes, but is not necessary.';
    'dataout', 'The predicted output measured data for this prediction.';
    'block_names', 'The names of the block/nodes/steps.';
    'inner_model_spec', 'A binary matrix specifying between which blocks the relationships are estimated. A "1" on [2, 1] means that a relationships from block 1 to block 2 is specified.';
    'outer_model_spec', 'A binary matrix specifying which measured variables correspond to which block. A "1" on position [4, 2] means that measured variable 4 belongs to block 2.';
    'blocks', 'A array that contains the different blocks (groups of variables modelled), and can contain the following sub-fields:';
        'blocks{m}.name', 'The name of the block/node/step.';
        'blocks{m}.type', 'The type of the block in the structure ("Start", "Middle" or "End").';
        'blocks{m}.X', 'The raw, unpreprocessed data for all measured variables that are part of this block.';
        'blocks{m}.Xpp', 'The preprocessed measured data for this block.';
        'blocks{m}.om_x', 'The (preprocessed) predictor data used for the PLS outer model fitted for this block.';
        'blocks{m}.om_y', 'The (preprocessed) response data used for the PLS outer model fitted for this block.';
        'blocks{m}.om_t', 'The X-scores for the PLS outer model fitted for this block.';
        'blocks{m}.om_u', 'The Y-scores for the PLS outer model fitted for this block.';
        'blocks{m}.im_x', 'The (preprocessed) predictor data used for the PLS inner model fitted for this block.';
    'rmse', 'The root mean square error for each predicted measured variable, in case the references were inputted.';
    'r2', 'The fraction of explained variance for each predicted measured variable, in case the references were inputted.';
    'timestamp', 'Date and time of modelling.';
    'info', 'Description of all fields and subfields of the model.'};

dataout = prediction.dataout;

end