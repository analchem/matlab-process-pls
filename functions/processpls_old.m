function model = processpls(data, im_specification, om_specification, varargin)

%DESCRIPTION:
%This function calculates a Process PLS model. Process PLS is a statistical
%method for path modelling (also known as structure equation modelling),
%that is optimized for use on industrial production data. Process PLS
%essentially calculates the statistical relationships between groups of
%measured variables (blocks), according to a user-imposed structure
%specification and using a network of Partial Least Squares (PLS) models. 
%It bears similarity to the more widely known Partial Least Squares Path 
%Modelling (PLS-PM). The method features two rounds of PLS-regressions, the
%first of which determines the latent variables for the different blocks of
%variables, and the second of which determines the regression (path)
%coefficients between those blocks of latent variables. For more details 
%on Process PLS, please see the original publication introducing it:
%
%van Kollenburg, Geert, et al. "Process PLS: Incorporating substantive 
%knowledge into the predictive modelling of multiblock, multistep, 
%multidimensional and multicollinear process data." Computers & Chemical 
%Engineering 154 (2021): 107466.
%
%This code should provide identical output as pathmodelr (version 0.1.2), 
%which is the original implementation of Process PLS in the R programming 
%language and which is shared on the Mendeley repository: 
%https://data.mendeley.com/datasets/9x9h7fr4kn/1
%
%Slight differences may occur due to the stochastic nature of the
%optimization of the number of latent variables in the Pathmodelr
%implementation.
%
%The code is furthermore organize and annotated to match the original 
%Process PLS publication (referenced above) as close as possible.
%Annotations that start at the beginning of a code line explain what the
%block of code lines below it do. Annotations starting after a line of code
%explain that the variable declared or changed in that specific line
%represents.
%
%INPUT:
%- data: The raw data for all measured variables that are modelled in this 
%  Process PLS model.
%- im_specification: A binary matrix specifying between which blocks the 
%  relationships are estimated. A '1' on [2, 1] means that a relationships 
%  from block 1 to block 2 is specified.
%- om_specification: A binary matrix specifying which measured variables 
%  correspond to which block. A '1' on position [4, 2] means that measured 
%  variable 4 belongs to block 2.
%
%The following name-value pairs can be used to provide additional modelling
%input:
%- block_names: A string array with the names of the block/nodes/steps.
%- nlvs: A vector with for each block the number of latent variables to
%  model. 'Inf' will model as many as possible. A negative number of -n
%  will auto-select the number of latent variables and considers at maximum
%  n latent variables. You can enter '-Inf', but this can be
%  computationally intensive for blocks with many variables. By default,
%  the number of latent variables is optimized with an internal 5-fold
%  Venetiab blinds cross-validation, but this can be changed with
%  additional name-value input pairs. Not that if a single number is
%  entered for "nlvs", then that number will be used for all blocks.
%- 'optscheme': Validation scheme to use for the optimization of the
%  number of LVs (both in outer and inner model optimizations):
%  - 'random' = Validation using a single random subset.
%  - 'kfold' = Random cross-validation using k folds.
%  - 'vblindsi' = Venetian blinds cross-validation on the sampling index
%    (recommended for time-series data, this is the default).
%  - 'vblindsy'= Venetian blinds cross-validation on the first column of Y.
%  - 'leaveout' Leave-one-out cross-validation.
%  - 'cblock' = Contiguous block cross-validation.
%- 'optn': Validation repeats to use (for deterministic schemes, the 
%  default = 1).
%- 'optk': For cross-validation, this is the number of partitions the 
%  data is divided into (default = 5). For single test set validation, a 
%  fraction of 1/k is taken out of the data for validation (so for k=5, 
%  20% of the data is used for testing.
%- 'signstable': Whether to use the signstable SVD by Rasmus Bro during PLS
%  optimization (https://doi.org/10.1002/cem.1122) or not (default).
%- 'algorithm': The algorithm to use for PLS model calculation: 'NIPALS' or 
%  'SIMPLS' (default).
%
%OUTPUT:
%- model: A Matlab-structure that represents the Process PLS model and has
%  the following fields:
%  - data: The raw data for all measured variables that are modelled in 
%    this Process PLS model.
%  - block_names: The names of the block/nodes/steps.
%  - im_specification: A binary matrix specifying between which blocks the 
%    relationships are estimated. A '1' on [2, 1] means that a relation-
%    ships from block 1 to block 2 is specified.
%  - om_specification: A binary matrix specifying which measured variables 
%    correspond to which block. A '1' on position [4, 2] means that 
%    measured variable 4 belongs to block 2.
%  - nlvs: The number of latent variables that Process PLS was asked to 
%    model, for each block. This might not be the actual number of 
%    latent variables used, those are saved per individual block.
%  - blocks: A array that contains the different blocks (groups of
%    variables modelled), and can contain the following sub-fields:
%    - name: The name of the block/node/step.
%    - type: The type of the block in the structure ('Start', 'Middle' or
%      'End').
%    - X: The raw, unpreprocessed data for all measured variables that are 
%      part of this block.
%    - Xpp: The preprocessed measured data for this block.
%    - om_x: The (preprocessed) predictor data used for the PLS outer model 
%      fitted for this block.
%    - om_y: The (preprocessed) response data used for the PLS outer model 
%      fitted for this block.
%    - om_lvs: The number of latent variables used by the PLS outer model 
%      fitted for this block.
%    - om_p: The X-loadings for the PLS outer model fitted for this block.
%    - om_r: The X-weights for the PLS outer model fitted for this block.
%    - om_t: The X-scores for the PLS outer model fitted for this block.
%    - om_q: The Y-loadings for the PLS outer model fitted for this block.
%    - om_u: The Y-scores for the PLS outer model fitted for this block.
%    - om_v: The fraction of explained variance in the predictor and 
%      response data by the PLS outer model fitted for this block.
%    - om_b: The regression vector for the PLS outer model fitted for this 
%      block.
%    - im_x: The (preprocessed) predictor data used for the PLS inner model 
%      fitted for this block.
%    - im_xb: The original blocks that each of the predictor variables 
%      originates from.
%    - im_y: The (preprocessed) response data used for the PLS inner model 
%      fitted for this block.
%    - im_lvs: The number of latent variables used by the PLS inner model 
%      fitted for this block.
%    - im_p: The X-loadings for the PLS inner model fitted for this block.
%    - im_r: The X-weights for the PLS inner model fitted for this block.
%    - im_t: The X-scores for the PLS inner model fitted for this block.
%    - im_q: The Y-loadings for the PLS inner model fitted for this block.
%    - im_u: The Y-scores for the PLS inner model fitted for this block.
%    - im_v: The fraction of explained variance in the predictor and 
%            response data by the PLS inner model fitted for this block.
%    - im_b: The regression vector for the PLS inner model fitted for this 
%      block.
%  - data_variances_explained: The fractions of explained variance by the
%    model in the measured data, per block. In the original Process PLS
%    paper, these are calculated as R^2 in section 2.2.3.
%  - path_variances_explained: The path coefficients in terms of fraction
%    of variance explained by the inner model. The structure of the matrix
%    follows that of the 'im_specification'. In the original Process PLS
%    paper, these are calcualted as P^2 in section 2.3.2.
%  - nlvs_modelled: The number of latent variables that was actually
%    calcualted used for each block in this Process PLS model.
%  - path_effects_direct: The direct effects of the latent variables of a
%    predictor block to the latent variables of a response block. In
%    Pathmodelr, these are stored as model$path_effects$direct.
%  - path_effects_indirect: The indirect effects of the latent variables of
%    a predictor block to the latent variables of a response block. In
%    Pathmodelr, these are stored as model$path_effects$indirect.
%  - path_effects_total: The sum of path_effects_direct and
%    path_effects_indirect, which in Pathmodelr are stored under
%    model$path_effects$total.
%  - outer_effects_on_LV: The effects of measured variables on the latent
%    variables that they represent. In Pathmodelr, they are stored as
%    model$outer_effects$outer_effects_on_LV.
%  - outer_effects: The effects of measured variables on their entire block
%    of latent variables collectively. In Pathmodelr. they are stored as
%    model$outer_effects$outer_effects.
%  - inner_effects_on_LV: The effects of measured variables on the latent
%    variables of other blocks that their block predicts. In Pathmodelr,
%    they are stores as model$inner_effect_on_LV.
%  - inner_effects: The effects of measured variables on other blocks that
%    their block predicts (all latent variables of that target block
%    combined). In Pathmodelr, these are stores under
%    model$outer_effects$outer_effects.
%  - addinput: The additional input that was parsed as name-value pairs
%    while calculating this specific model.
%  - timestamp: Date and time of modelling.
%  - info: Description of all fields and subfields of the model.
%
%AUTHOR:
%Tim Offermans, Radboud University Nijmegen (The Netherlands), October 2021
%
%VERSION:
%All codes tested on Matlab R2021a.
%
%SYNTAX:
%model = processpls(data, im_specification, om_specification, varargin, 'nlvs', Inf)

%CHECK FUNCTION INPUT
%Fill in default block names. These are kept if none are given, and
%overwritten if they are.
block_names = string(strcat('Block_', num2str((1:length(im_specification))')));
%Fill in the default number of variables to model, which is as to
%auto-select the number without limitations using cross-validation:
nlvs = -Inf;
optscheme = 'vblindsi';
optn = 1;
optk = 5;
signstable = true;
algorithm = 'SIMPLS';
%Parse name-value pairs for additional modelling input:
for i=1:2:length(varargin)
    eval([varargin{i} ' = varargin{i+1};']);
end
%Make inner and outer model specification matrices boolean (true/false), 
%in case they are not:
im_specification = logical(im_specification);
om_specification = logical(om_specification);
%In case the number of LVs to model is only given as a single number, copy
%it so that it is available per block modelled:
if length(nlvs)<length(im_specification)
    nlvs = repmat(nlvs(1), length(im_specification), 1);
end

%INITIALIZE MODEL STRUCTURE
model = [];
model.data = data; %The raw data for all measured variables that are modelled in this Process PLS model. In the paper, this is the N-by-P matrix V in section 2.1.1.
model.block_names = block_names; %The names of the block/nodes/steps. In pathmodelr, this is 'model$block_names'. In the paper, there are a number of M blocks.
model.im_specification = im_specification; %A binary matrix specifying between which blocks the relationships are estimated. A '1' on [2, 1] means that a relationships from block 1 to block 2 is specified. In pathmodelr, this is 'model$connection_matrix'. In the paper, this is the lower-triangular matrix C of size M-by-M in section 2.1.2.
model.om_specification = om_specification; %A binary matrix specifying which measured variables correspond to which block. A '1' on position [4, 2] means that measured variable 4 belongs to block 2. In the paper, this specification is in section 2.1.1, but no matrix is defined.
model.nlvs = nlvs; %The number of latent variables that Process PLS was asked to model, for each block. This might not be the actual number of latent variables used, those are saved per individual block.

%INITIALIZE BLOCKS
%As in pathmodelr, the model is organized into the M different data
%blocks/nodes/steps that are specified. This part of the code initializes
%those blocks and extracts the measured data belonging to it from the main
%data matrix (V in the paper). 'model.blocks{1}' in this code corresponds 
%to 'model$nodes[[1]]' in pathmodelr.
model.blocks = cell(length(model.block_names), 1);
%Fill data per block:
for b=1:length(model.blocks)
    model.blocks{b}.name = model.block_names(b); %The name of the block/node/step. In pathmodelr, this is 'model$nodes$...$node_name'.
    %The type of the block is defined below, and is either 'Start' (block 
    %predicts but is not predicted), 'Middle' (block predicts and is also 
    %predicted) or 'End' (block does not predict but is predicted):
    if ~any(model.im_specification(b, :))
        model.blocks{b}.type = 'Start';
    elseif ~any(model.im_specification(:, b))
        model.blocks{b}.type = 'End';
    else
        model.blocks{b}.type = 'Middle';
    end
    model.blocks{b}.X = model.data(:, model.om_specification(:, b)); %The raw, unpreprocessed data for all measured variables that are part of this block. In pathmodelr, this is ('model$nodes$...$X_data').
end

%DATA PREPROCESSING
%The raw data is centered and scaled in three steps, as described in 
%Appendix A section A1 of the paper. This is done per block individually.
for b=1:length(model.blocks)
    %Copy the raw data:
    model.blocks{b}.Xpp = model.blocks{b}.X; %This will contain the preprocessed measured data for this block. In pathmodelr, the preprocessed data per block is stored in 'model$nodes$...$preprocessed_X'.
    %Mean-center each individual measured variable (by subtracting the mean of the variable):
    model.blocks{b}.Xpp = model.blocks{b}.Xpp - (ones(size(model.blocks{b}.Xpp, 1), 1) * mean(model.blocks{b}.Xpp));
    %Autoscale each individual variable (by dividing by the standard deviation of the variable):
    model.blocks{b}.Xpp = model.blocks{b}.Xpp ./ (ones(size(model.blocks{b}.Xpp, 1), 1) * std(model.blocks{b}.Xpp));
    %Block-scale all measured variables of this block, so that they
    %together have a sum of squares of 1.
    model.blocks{b}.Xpp = model.blocks{b}.Xpp ./ sqrt(mean((model.blocks{b}.Xpp(:).^2) * size(model.blocks{b}.Xpp, 2)));
    
end
    
%OUTER MODEL ESTIMATION
%The latent variables for each block that make up the outer model of
%the Process PLS model are calculated per block. Note that this code
%saves more information of the model than the pathmodelr-code.
%Pathmodelr rescales and saves only the X scores and loadings in case
%the block is a predictor block, and it rescales and saves only the Y
%scores and loadings in case the block is target block only. This
%Matlab-code saves the scores and loadings for both X and Y irrespective 
%of the block type, and rescales both of them (on explained variance and 
%block variance). In the paper, the outer model estimation is explained in 
%section 2.2. The same symbols are used in this Matlab code as in that 
%section 2.2.
for b=1:length(model.blocks)
    %Initialize all output:
    model.blocks{b}.om_x = []; %The (preprocessed) predictor data used for the PLS outer model fitted for this block.
    model.blocks{b}.om_y = []; %The (preprocessed) response data used for the PLS outer model fitted for this block.
    model.blocks{b}.om_lvs = []; %The number of latent variables used by the PLS outer model fitted for this block. In pathmodelr, this is 'model$nodes$...$n_LVs'.
    model.blocks{b}.om_p = []; %The X-loadings for the PLS outer model fitted for this block.
    model.blocks{b}.om_r = []; %The X-weights for the PLS outer model fitted for this block. In pathmodelr, this is 'model$nodes$...$X_loadings'.
    model.blocks{b}.om_t = []; %The X-scores for the PLS outer model fitted for this block. In pathmodelr, this is 'model$nodes$...$LVs'.
    model.blocks{b}.om_q = []; %The Y-loadings for the PLS outer model fitted for this block.
    model.blocks{b}.om_u = []; %The Y-scores for the PLS outer model fitted for this block.
    model.blocks{b}.om_v = []; %The fraction of explained variance in the predictor and response data by the PLS outer model fitted for this block.
    model.blocks{b}.om_b = []; %The regression vector for the PLS outer model fitted for this block.
	%Find the predictor and response data in case this block has target 
    %blocks and thus is a predictor block (in the paper, this situation is 
    %described in section 2.2.1):
	if any(model.im_specification(:, b))
        %For such a block, the predictor data is the data measured for that
        %block m itself:
        model.blocks{b}.om_x = model.blocks{b}.Xpp;
        %The response data is then the  (concatenated) data measured for 
        %all blocks that block m has a relationship to:
        for i=find(model.im_specification(:, b)')
            model.blocks{b}.om_y = [model.blocks{b}.om_y, model.blocks{i}.Xpp];
        end
        %Select the highest number of latent variables possible:
        model.blocks{b}.om_lvs = min(size(model.blocks{b}.om_x));
        %If fewer latent variables were aksed than the highest number
        %possible, use that instead:
        if model.nlvs(b)<model.blocks{b}.om_lvs
            model.blocks{b}.om_lvs = model.nlvs(b);
        end
    %Find the predictor and response data in case this block has no target 
    %blocks and thus is a target block only (in the paper, this situation
    %is described in section 2.2.2):
    else
        %For such a block, the predictor data is the (concatenated) data 
        %measured for all blocks that have an incoming relationship to 
        %block m:
        for i=find(model.im_specification(b, :))
            model.blocks{b}.om_x = [model.blocks{b}.om_x, model.blocks{i}.Xpp];
        end
        %The response data is then measured data for that block m itself:
        model.blocks{b}.om_y = model.blocks{b}.Xpp;
        %Select the highest number of latent variables possible.
        model.blocks{b}.om_lvs = size(model.blocks{b}.om_x, 2);
        %If fewer latent variables were aksed than the highest number 
        %possible, use that instead.
        if model.nlvs(b)<model.blocks{b}.om_lvs
            model.blocks{b}.om_lvs = model.nlvs(b);
        end
    end
    %Fit the outer PLS model and extract all modelling information. By
    %default, the sign-stable SVD and SIMPLS are used (as is described in
    %the paper in section 2.2), but these settings may be overidden:
    [B, T, U, P, Q, V, W] = pls_lvoptim(model.blocks{b}.om_x, model.blocks{b}.om_y, 0, 0, model.blocks{b}.om_lvs, 'signstable', signstable, 'algorithm', algorithm, 'optscheme', optscheme, 'optn', optn, 'optk', optk);
    %Extract all modelling information:
    model.blocks{b}.om_p = P;
    model.blocks{b}.om_r = W;
    model.blocks{b}.om_t = T;
    model.blocks{b}.om_q = Q;
    model.blocks{b}.om_u = U;
    model.blocks{b}.om_v = V;
    model.blocks{b}.om_b = B;
    %Save the number of latent variables that was actuall selected as
    %optimal for the outer PLS model for this block:
    model.blocks{b}.om_lvs = size(T, 2);
    %Rescale the X-scores and X-weights for the latent variables according
    %to the fraction of variances explained by the latent variables in the 
    %(preprocessed) measured variables. This is described in the paper in
    %Appendix A in section A1. As in pathmodelr, the scaling factor is
    %calculated per latent variable and is composed of a variance-scaling
    %factor and a block-scaling factor.
    s = ones(1, size(model.blocks{b}.om_v, 2)); %Initialize scaling factor
    s = s .* sqrt(s .* model.blocks{b}.om_v(1, :)); %Add variance-scaling factor
    s = s ./ sqrt(sum((model.blocks{b}.om_t * s').^2)); %Add block-scaling factor
    model.blocks{b}.om_t = model.blocks{b}.om_t * diag(s); %The rescaled X-scores. In pathmodelr, this is 'model$nodes$...$LVs' when the block is a predictor block.
    model.blocks{b}.om_r = model.blocks{b}.om_r * diag(s); %The rescaled X-weights. In pathmodelr, this is 'model$nodes$...$X_loadings' when the block is a predictor block.
    %Rescale the Y-scores and Y-weights for the latent variables according
    %to the fraction of variances explained by the latent variables in the 
    %(preprocessed) measured variables. This is described in the paper in
    %Appendix A in section A1. As in pathmodelr, the scaling factor is
    %calculated per latent variable and is composed of a variance-scaling
    %factor and a block-scaling factor.
    s = ones(1, size(model.blocks{b}.om_v, 2)); %Initialize scaling factor
    s = s .* sqrt(s .* model.blocks{b}.om_v(2, :)); %Add variance-scaling factor
    s = s ./ sqrt(sum((model.blocks{b}.om_u * s').^2)); %Add block-scaling factor
    model.blocks{b}.om_u = model.blocks{b}.om_u * diag(s); %The rescaled X-scores. In pathmodelr, this is 'model$nodes$...$LVs' when the block is a target block.
    model.blocks{b}.om_q = model.blocks{b}.om_q * diag(s); %The rescaled X-weights. In pathmodelr, this is 'model$nodes$...$X_loadings' when the block is a target block.
    %Note that in pathmodelr, the information in 
    %'model$outer_effects$outer_effects_on_LV' corresponds in this Matlab 
    %code to model.blocks{m}.om_r for predictor blocks and to
    %model.blocks{m}.om_q for target blocks.
end
%Note: The outer model estimation as performed by this Matlab code gives 
%results identical to those obtained by pathmodelr. The only difference is 
%(sometimes) the sign of the latent variables, which is caused by the 
%well-known problem of sign indeterminancy of PLS. To cope with this sign 
%indeterminancy, it is proposed for Process PLS to use Rasmus Bro's method
%for determining the sign of the LVs (in the paper this is discussed in
%Appendix A in section A3). However, the original Process PLS
%implementation in R (pathmodelr) does not use this method, while it is
%implemented in this Matlab-code. For this reason, the LV signs may still
%be different between the Matlab and original R implementation.
    
%INNER MODEL ESTIMATION
%The inner model is estimated by a second round of PLS-regressions, which 
%are again performed per (target) block. The latent variables of each block 
%are regressed on the combined latent variables of all blocks predicting 
%that target block. The latent variables of the block itself are thus used 
%as dependent/response matrix; the latent variables of all incoming blocks 
%are used as independent/predictor matrix. For a starting block without any 
%incoming relationships, no model is calibrated (as there is no dependent 
%data). In the paper, this methodology is described in section 2.3.
lvs = [0 2 4 6 7];
for b=1:length(model.blocks)
    %A PLS regression is only applicable if this block is a target block
    %and has at least one incoming relationship.
    if any(model.im_specification(b, :))
        %Initialize all output:
        model.blocks{b}.im_x = []; %The (preprocessed) predictor data used for the PLS inner model fitted for this block.
        model.blocks{b}.im_xb = []; %The original blocks that each of the predictor variables originates from, which is convenient to calculate the inner path coefficients later on.
        model.blocks{b}.im_y = []; %The (preprocessed) response data used for the PLS inner model fitted for this block.
        model.blocks{b}.im_lvs = []; %The number of latent variables used by the PLS inner model fitted for this block.
        model.blocks{b}.im_p = []; %The X-loadings for the PLS inner model fitted for this block.
        model.blocks{b}.im_r = []; %The X-weights for the PLS inner model fitted for this block.
        model.blocks{b}.im_t = []; %The X-scores for the PLS inner model fitted for this block.
        model.blocks{b}.im_q = []; %The Y-loadings for the PLS inner model fitted for this block.
        model.blocks{b}.im_u = []; %The Y-scores for the PLS inner model fitted for this block.
        model.blocks{b}.im_v = []; %The fraction of explained variance in the predictor and response data by the PLS inner model fitted for this block.
        model.blocks{b}.im_b = []; %The regression vector for the PLS inner model fitted for this block.            
        %Find the predictor data for this PLS inner model:
        for i=find(model.im_specification(b, :))
            model.blocks{b}.im_x = [model.blocks{b}.im_x, model.blocks{i}.om_t];
            model.blocks{b}.im_xb = [model.blocks{b}.im_xb, repmat(i, 1, size(model.blocks{i}.om_t, 2))];
        end
        %If the current block predicts other blocks, the outer X-scores
        %can be used as inner response data:
        if any(model.im_specification(:, b))
            model.blocks{b}.im_y = model.blocks{b}.om_t;
        %Else, if the current block is a target-only block (end node),
        %the outer Y scores should be used as inner response data:
        else
            model.blocks{b}.im_y = model.blocks{b}.om_u;
        end
        %For the PLS inner models, as many latent variables as 
        %possible will be modelled:
        model.blocks{b}.im_lvs = size(model.blocks{b}.im_x, 2);
        %Fit the outer PLS model and extract all modelling information. 
        %By default, the sign-stable SVD and SIMPLS are used (as is 
        %described in the paper in section 2.2), but these settings 
        %may be overidden:
        [B, T, U, P, Q, V, W] = pls_lvoptim(model.blocks{b}.im_x, model.blocks{b}.im_y, 0, 0, -model.blocks{b}.im_lvs, 'signstable', signstable, 'algorithm', algorithm, 'optscheme', optscheme, 'optn', optn, 'optk', optk);
        %Extract all modelling information:
        model.blocks{b}.im_p = P;
        model.blocks{b}.im_r = W;
        model.blocks{b}.im_t = T;
        model.blocks{b}.im_q = Q;
        model.blocks{b}.im_u = U;
        model.blocks{b}.im_v = V;
        model.blocks{b}.im_b = B;
        %Save the number of latent variables that was actuall selected as
        %optimal for the inner PLS model for this block:
        model.blocks{b}.im_lvs = size(T, 2);
    end
end

%EXPLAINED VARIANCE OF MEASURED DATA
%Investigating the fraction of variance explained in the measured data is
%useful to judge how well the Process PLS model describes the data
%modelled. A better description of the measured data will lead to a better
%estimation of path effects. These fractions of explained variance are
%saved in the seperate blocks, but are copied to the main model-structure 
%for overview, and so that it can be easily bootstrapped by the supplied 
%bootstrapping routine for Process PLS. In the original Process PLS paper, 
%they are calcualted as R2 in section 2.2.3. In the original pathmodelr
%implementation, they are not stored in the main structure but also only
%per block ('node').
model.data_variances_explained = NaN(length(model.block_names), 1);
for b=1:length(model.block_names)
    %In case the block is predicting, save the explained variance in X:
    if any(model.im_specification(:, b))
        model.data_variances_explained(b) = sum(model.blocks{b}.om_v(1, :));
    %Else, in case the block is target-only, save the explained variance in
    %Y:
    else
        model.data_variances_explained(b) = sum(model.blocks{b}.om_v(2, :));
    end    
end

%PATH COEFFICIENT CALCULATION
%Now that all PLS models are fitted (both for the inner and outer model
%estimation), all path coefficients can be calculated from them. These
%are essentially the fraction of variance in a target block that is
%explained by a predictor block, following the relationships specified
%in the inner model specification. In the paper, these are stored in the
%matrix P2(m, z) described in section 2.3.2. In pathmodelr, this matrix is
%stored as 'model$path_variances_explained'.
model.path_variances_explained = double(model.im_specification);
%Loop through all blocks as being target blocks:
for t=1:size(model.im_specification, 1)
    %Only calculate the relationships to this block if it actually is a
    %target block and has incoming relationships:
    if any(model.im_specification(t, :))
        Y = model.blocks{t}.im_y; %The reference latent variables scores of the target block t that are being predicted from all predictors blocks together. In the paper, this is matrix Xi_hat.
        Y_pred = zeros(size(Y)); %Initialize the latent variables scores of the target block t as predicted by the latent variable scores of all predictor blocks together. In the paper this is the product of the matrices Chi and B.
        Y_pred_part = NaN(size(Y, 1), size(Y, 2), size(model.im_specification, 1)); %The partial predicted latent variables scores of the target block t, separately for each of the predictor blocks.
        E_pred_part = NaN(size(Y, 1), size(Y, 2), size(model.im_specification, 1)); %The errors of the partial predicted latent variables scores of the target block t, separately for each of the predictor blocks.
        Y_pred_part_sum = 0; %The sum of squares of the predicted latent variables scores of the target block t.
        %For target block t, the incoming relationships for all predictor
        %blocks f are calculated individually:
        for f=1:size(model.im_specification, 1)
            %Only calculate the relationships from block f to block t if
            %there is one specifiec in the inner model specification:
            if model.im_specification(t, f)
                B = model.blocks{t}.im_b(find(model.blocks{t}.im_xb==f)+1, :); %The regression coefficients for predicting the latent varibales scores of the (target) block t from predictor block f.
                X_LV = model.blocks{t}.im_x(:, find(model.blocks{t}.im_xb==f)); %The scores of the predicting block f that are used to predict the scores of the target block t.
                Y_pred_part(:, :, f) = X_LV * B; %Estimate the scores of the target block t from the predicting block f, and save as partial prediction.
                Y_pred = Y_pred + Y_pred_part(:, :, f); %Add the partial prediction from block f to the total prediction.
                E_pred_part(:, :, f) = Y - Y_pred_part(:, :, f); %Calculate the partial prediction error from block f to block t.
                Y_pred_part_sum = Y_pred_part_sum + sum(Y_pred_part(:, :, f).^2, 'all'); %Update the sum of squares for the prediction of block t from block f specifically.
            end
        end
        E_pred = Y-Y_pred; %Calculate the total prediction error for the latent variables of block t from all incoming blocks.
        %Now that the partial predictions (from the separate predicting
        %blocks) and the total prediction (from all predicting blocks
        %together) for the latent variables of block t are calculated, the
        %actual explained vairiances can be calculated. These are the
        %partial explained variances, and are thus calculated per
        %predicting block f:
        for f=1:size(model.im_specification, 1)
            %Only calculate the (partial) explained variance from block f
            %to block t if there is a relationship specified in the inner
            %model specification:
            if model.im_specification(t, f)
                %Calculate the partial explained variance. In the paper,
                %this is the last equation introduced in section 2.3.2:
                model.path_variances_explained(t, f) = (1-(sum(E_pred.^2, 'all') / sum(Y.^2, 'all'))) * (sum(Y_pred_part(:, :, f).^2, 'all') / Y_pred_part_sum);
            end
        end
    end
end

%NUMBER OF LATENT VARIABLES MODELLED
%The number of latent variables that are actually caclulated and used for
%the Process PLS model may be different than the input, as the input may
%ask the algorithm to optimize the number of latent variables. The actual
%number of latent variables calculated is stored in each of the blocks, but
%is copied to the main model-structure for overview, and so that it can be
%easily bootstrapped by the supplied bootstrapping routine for Process PLS.
model.nlvs_modelled = NaN(length(model.block_names), 1);
for b=1:length(model.block_names)
    model.nlvs_modelled(b) = model.blocks{b}.om_lvs;
end

%PATH EFFECT CALCULATION
%In this part, the path effects are calculated for the fitted model. These
%represent the effects of the latent variables in predictor blocks on the
%latent variables of response blocks. They are calculated based on the
%regression coefficient between those latent variables as obtained during
%inner model estimation. The path effects are stored similar to the inner
%model connectivity matrix. However, because multiple latent variables can
%be modelled per block, each cell in this matrix is a sub-matrix. First,
%the direct effects between predictor and response blocks are obtained as
%the regression coefficients of the inner PLS models. These are not
%mentioned in the original Process PLS paper, but are stored as
%model$path_effects$direct in the original Pathmodelr implementation in R.
model.path_effects_direct = cell(size(im_specification)); %Initialize
for f=1:size(im_specification, 2) %Loop through predictor blocks
    for t=f+1:size(im_specification, 2) %Loop through the response blocks
        if any(im_specification(t, f)) %Only calculate when there is a relationship
            model.path_effects_direct{t, f} = model.blocks{t}.im_b(find(model.blocks{t}.im_xb==f)+1, :);
        end
    end
end
%The indirect path effects are calculated based on the direct effects. This 
%is done by finding all indirect path effects between two blocks. Per path,
%the indirect effects are calculated by multiplying the regression
%coefficients along the path. Finally, all possible indirect effects
%between two blocks are summed. In the original Pathmodelr implementation
%in R, these are stored as model$path_effects$indirect.
model.path_effects_indirect = cell(size(im_specification)); %Initialize
for f=1:size(im_specification, 2) %Loop through predictor blocks
    for t=f+2:size(im_specification, 2) %Loop through the response blocks
        if any(im_specification(t, f)) %Only calculate when there is a relationship
            %Find all possible indirect paths between these two blocks if 
            %the connection matrix would be a filled lower-triangular 
            %matrix:
            P = f:t;
            F = fullfact(repmat(2, 1, t-f-1))-1;
            F(sum(F, 2)==0, :) = [];
            P = repmat(P, size(F, 1), 1);
            P(:, 2:end-1) = P(:, 2:end-1) .* F;
            P = num2cell(P, 2);
            %Then, calculate the indirect effects over all possible paths.
            %If a path is not possible because the actual connection matrix
            %is not a completely filled lower-triangular matrix, it is
            %discarded.
            E = cell(size(P));
            for p=1:length(P)
                P{p}(P{p}==0) = [];
                E{p} = 1;
                for i=2:length(P{p})
                    if im_specification(P{p}(i), P{p}(i-1))
                        E{p} = E{p} * model.path_effects_direct{P{p}(i), P{p}(i-1)};
                    else
                        E{p} = [];
                        break
                    end
                end
            end
            %Sum the indirect effects over all possible paths to get the 
            %final indirect effects between these two blocks:
            model.path_effects_indirect{t, f} = 0;
            E(cellfun('isempty', E)) = [];
            for e=1:length(E)
                model.path_effects_indirect{t, f} = model.path_effects_indirect{t, f} + E{e};
            end
        end
    end
end
%Finally, the total path effects are calculated as the sum of direct and
%total effects. In the original Pathmodelr implementation in R, these are 
%stored as model$path_effects$total.
model.path_effects_total = cell(size(im_specification)); %Initialize
for f=1:size(im_specification, 2) %Loop through predictor blocks
    for t=f+1:size(im_specification, 1) %Loop through the response blocks
        if any(im_specification(t, f)) %Only calculate when there is a relationship
            model.path_effects_total{t, f} = model.path_effects_direct{t, f};
            if ~isempty(model.path_effects_indirect{t, f})
                model.path_effects_total{t, f} = model.path_effects_total{t, f} + model.path_effects_indirect{t, f};
            end
        end
    end
end

%OUTER EFFECT CALCULATION:
%This part will calculate the outer effects of the model. These are the
%effects of measured variables on the latent variables of the block they
%represent. They are calculated based on the outer model loadings as
%obtained during outer model estimation. First, the effects of the measured
%variables on each individual latent variable are extracted. They are
%oranized as the outer model connectivity matrix, but as each block may
%contain multiple latent variables, each cell in this matrix is a 
%sub-matrix. The calculation of outer effects is not mentioned in the
%original Process PLS publication, but in the original Pathmodelr
%implementation in R they are stored as
%model$outer_effects$outer_effects_on_LV.
model.outer_effects_on_LV = cell(size(om_specification)); %Initialize
for b=1:size(om_specification, 2) %Loop through the blocks
    %If the block is predicting other blocks, these are the X-loadings:
    if any(model.im_specification(:, b))
        model.outer_effects_on_LV(om_specification(:, b), b) = num2cell(model.blocks{b}.om_r, 2);
    %Else, if the block is a target-block only, these are the Y-loadings:
    else
        model.outer_effects_on_LV(om_specification(:, b), b) = num2cell(model.blocks{b}.om_q, 2);
    end
end
%Next, the effects of the measured variables on their entire blocks (so the
%latent variables combined) are calculated. They are effectively the sum of
%the weights per latent variable as obtained above, but this sum is
%weighted according to the fraction of variance each latent variables
%explained in all measured variables that they represent. In the original
%Pathemodelr implementation in R, they are stored as
%model$outer_effects$outer_effects.
model.outer_effects = zeros(size(om_specification)); %Initialize
for b=1:size(om_specification, 2) %Loop through the blocks
    %If the block is predicting other blocks, we should use the X-loadings:
    if any(model.im_specification(:, b))
        r = model.blocks{b}.om_r;
        v = model.blocks{b}.om_v(1, :);
    %Else, if the block is a target-block only, we use the Y-loadings:
    else
        r = model.blocks{b}.om_q;
        v = model.blocks{b}.om_v(2, :);
    end
    model.outer_effects(om_specification(:, b), b) = sum((r .* repmat(v, size(r, 1), 1)), 2) ./ sum(v, 2);
end

%INNER EFFECT CALCULATION:
%Finally, the outer effects are calculated. These are the effects of the 
%measured in other blocks than the one they are part of themselves (further 
%up ahead). They are essentially calculated by multiplying the outer model 
%loadings of the measured variables in their own block with the path 
%effects (as calculated above) between that block and the another block 
%(further up ahead). Just like the outer effects, these are first 
%calculated for each latent variable of another block separately, and are
%then summed to obtain the inner effect of each measured variable on the
%combined latent variables of another block. In the original Pathmodelr
%implementation in R, these are stored respectivelt as
%model$inner_effects$inner_effects_on_LV and
%model$inner_effects$inner_effects.
model.inner_effects_on_LV = cell(size(om_specification)); %Initialize
for f=1:size(im_specification, 1) %Loop through predictor blocks
    for t=f+1:size(im_specification, 1) %Loop through response blocks
        if im_specification(t, f) %Only calculate when there is a relationship
            model.inner_effects_on_LV(om_specification(:, f), t) = num2cell(model.blocks{f}.om_r * model.path_effects_total{t, f}, 2);
        end
    end
end
%Sum the outer effects of measured variables on other latent variables, to
%obtain the outer effects of measured variables on other blocks
%collectively:
model.inner_effects = zeros(size(om_specification)); %Initialize
for v=1:size(om_specification, 1) %Loop through measured variables
    for t=1:size(om_specification, 2) %Loop through target blocks
        model.inner_effects(v, t) = sum(model.inner_effects_on_LV{v, t});
    end
end

%MODEL OUTPUT PREPARATION
%This part of the code will finalize the modelling output by adding the
%additional input parsed as name-value pairs while calculating this model,
%an information-field with a description of the output structure, as well 
%as a timestamp.
model.addinput = reshape(varargin, 2, length(varargin)/2)';
model.timestamp = datetime();
model.info = {
    'description', 'This Matlab-structure represents a Process PLS model and has the below (sub)fields.';
    'reference','van Kollenburg, Geert, et al. "Process PLS: Incorporating substantive knowledge into the predictive modelling of multiblock, multistep, multidimensional and multicollinear process data." Computers & Chemical Engineering 154 (2021): 107466.';
    'data', 'The raw data for all measured variables that are modelled in this Process PLS model.';
    'block_names', 'The names of the block/nodes/steps.';
    'im_specification', 'A binary matrix specifying between which blocks the relationships are estimated. A "1" on [2, 1] means that a relationships from block 1 to block 2 is specified.';
    'om_specification', 'A binary matrix specifying which measured variables correspond to which block. A "1" on position [4, 2] means that measured variable 4 belongs to block 2.';
    'nlvs', 'The number of latent variables that Process PLS was asked to model, for each block. This might not be the actual number of latent variables used, those are saved per individual block. If the number is Infinite, then Process PLS considered as many LVs as possible. If the number if negative, Process PLS auto-optimzed the number of LVs with that absolute number as maximum.';
    'blocks', 'A array that contains the different blocks (groups of variables modelled), and can contain the following sub-fields:';
        'blocks{m}.name', 'The name of the block/node/step.';
        'blocks{m}.type', 'The type of the block in the structure ("Start", "Middle" or "End").';
        'blocks{m}.X', 'The raw, unpreprocessed data for all measured variables that are part of this block.';
        'blocks{m}.Xpp', 'The preprocessed measured data for this block.';
        'blocks{m}.om_x', 'The (preprocessed) predictor data used for the PLS outer model fitted for this block.';
        'blocks{m}.om_y', 'The (preprocessed) response data used for the PLS outer model fitted for this block.';
        'blocks{m}.om_lvs', 'The number of latent variables used by the PLS outer model fitted for this block.';
        'blocks{m}.om_p', 'The X-loadings for the PLS outer model fitted for this block.';
        'blocks{m}.om_r', 'The X-weights for the PLS outer model fitted for this block.';
        'blocks{m}.om_t', 'The X-scores for the PLS outer model fitted for this block.';
        'blocks{m}.om_q', 'The Y-loadings for the PLS outer model fitted for this block.';
        'blocks{m}.om_u', 'The Y-scores for the PLS outer model fitted for this block.';
        'blocks{m}.om_v', 'The fraction of explained variance in the predictor and response data by the PLS outer model fitted for this block.';
        'blocks{m}.om_b', 'The regression vector for the PLS outer model fitted for this block.';
        'blocks{m}.im_x', 'The (preprocessed) predictor data used for the PLS inner model fitted for this block.';
        'blocks{m}.im_xb', 'The original blocks that each of the predictor variables originates from.';
        'blocks{m}.im_y', 'The (preprocessed) response data used for the PLS inner model fitted for this block.';
        'blocks{m}.im_lvs', 'The number of latent variables used by the PLS inner model fitted for this block.';
        'blocks{m}.im_p', 'The X-loadings for the PLS inner model fitted for this block.';
        'blocks{m}.im_r', 'The X-weights for the PLS inner model fitted for this block.';
        'blocks{m}.im_t', 'The X-scores for the PLS inner model fitted for this block.';
        'blocks{m}.im_q', 'The Y-loadings for the PLS inner model fitted for this block.';
        'blocks{m}.im_u', 'The Y-scores for the PLS inner model fitted for this block.';
        'blocks{m}.im_v', 'The fraction of explained variance in the predictor and response data by the PLS inner model fitted for this block.';
        'blocks{m}.im_b', 'The regression vector for the PLS inner model fitted for this block.';
    'data_variances_explained', 'The fractions of explained variance by the model in the measured data, per block. In the original Process PLS paper, these are calculated as R^2 in section 2.2.3.';
    'path_variances_explained', 'The path coefficients in terms of fraction of variance explained by the inner model. The structure of the matrix follows that of the "im_specification" In the original Process PLS paper, these are calcualted as P^2 in section 2.3.2..';
    'nlvs_modelled', 'The number of latent variables that was actually calcualted used for each block in this Process PLS model.';
    'path_effects_direct', 'The direct effects of the latent variables of a predictor block to the latent variables of a response block. In Pathmodelr, these are stored as model$path_effects$direct.';
    'path_effects_indirect', 'The indirect effects of the latent variables of a predictor block to the latent variables of a response block. In Pathmodelr, these are stored as model$path_effects$indirect.';
    'path_effects_total', 'The sum of path_effects_direct and path_effects_indirect, which in Pathmodelr are stored under model$path_effects$total.';
    'outer_effects_on_LV', 'The effects of measured variables on the latent variables that they represent. In Pathmodelr, they are stored as model$outer_effects$outer_effects_on_LV.';
    'outer_effects', 'The effects of measured variables on their entire block of latent variables collectively. In Pathmodelr. they are stored as model$outer_effects$outer_effects.';
    'inner_effects_on_LV', 'The effects of measured variables on the latent variables of other blocks that their block predicts. In Pathmodelr, they are stores as model$inner_effect_on_LV.';
    'inner_effects', 'The effects of measured variables on other blocks that their block predicts (all latent variables of that target block combined). In Pathmodelr, these are stores under model$outer_effects$outer_effects.';
    'addinput', 'The additional input that was parsed as name-value pairs while calculating this specific model.';    
    'addinput', 'The additional input that was parsed as name-value pairs while calculating this specific model.';
    'timestamp', 'Date and time of modelling.';
    'info', 'Description of all fields and subfields of the model.'};

%NOTE BY THE AUTHOR:
%On October 8th, 2021, some of the output fields of the general
%model-structure were renamed to match the names in a newer version of
%pathmodelr in R. The changes are given below. If some functions (such as
%plotting) are not working for you, it might be that you have some
%functions renames, but not all.
%- data_variance_explained = expvar_in_MV_block
%- path_variances_explained -> inner_model
%- path_effects_direct -> LV_on_LV_direct
%- path_effects_indirect -> LV_on_LV_indirect
%- path_effects_total -> LV_on_LV_total
%- inner_effects_on_LV -> MV_on_other_LV
%- inner_effects -> MV_on_other_block
%- outer_effects_on_LV = loadings
%- outer_effects -> variable_contribution

end