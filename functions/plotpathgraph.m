function plotpathgraph(model, bootstrap, varargin)

%DESCRIPTION:
%A function to plot path graphs for path modelling results. The function
%was specifically written for output of 'processpls.m', but can also be
%used for outer path modelling software if the results are properly parsed. 
%It will plot the inner model structure, and (if available) the path 
%effects between the relationships that constitute the inner model. These 
%are quantified as the fraction of explained variance in each latent 
%variable block, denoted as P^2 in the original Process PLS paper and the
%field 'path_variance_explained' in the model-structure that processpls.m
%outputs.
%
%INPUT:
%- model: The path model structure. Preferably, this is the output of
%  processpls.m, but structure generated differently will also be accepted
%  as long as it has the following fields:
%  - inner_model_spec: A binary matrix specifying between which blocks the 
%    relationships are estimated. A '1' on [2, 1] means that a 
%    relationships from block 1 to block 2 is specified.
%  - block_names: The names of the block/nodes/steps.
%  and possibly the field:
%  - inner_model: The path coefficients in terms of fraction
%    of variance explained by the inner model. The structure of the matrix
%    follows that of the 'inner_model_spec'.
%- bootstrap: Whether to show the mean of the bootstrapped modelling
%  results, or the non-bootstrapped modelling results (given that they are
%  available). This can only be used when the boostrap-results from
%  'processpls_boot.m' are present as the field 'bootstrap' in the
%  model-structure.
%
%- The following name-value pairs can be used:
%- 'radius': Circle radius for the blocks (default: 0.25).
%- 'facecolor': Circle fill color for the blocks (default: [0.75 0.75 0.75]).
%- 'fontsize': Font size for the text (default: 10).
%- 'fontname': Font name for the text (default: 'Century Gothic')
%- 'plottitle': Title of the plot.
%- 'plotsize': Size of the plot canvas.
%- 'fontcolor': Color of the font (applies only to the block names, default: [0 0 0]).
%- 'visible': Set the visibility of the plot 'on' (default) or 'off'. This
%  is useful for scripts were many plots are made and saved to the disk,
%  but should not clutter the screen. Figures that are not visible should
%  however still be closed to save memory ('close gcf').
%- 'xy': By default, the blocks are plotted over an evenly-spread ellipse.
%  You can also give other coordinates with this input argument. This
%  should be a number-of-blocks by 2 matrix, the columns being x- and
%  y-coordinates, respectively.
%
%AUTHOR:
%Tim Offermans, Radboud University Nijmegen (The Netherlands), October 2021
%
%VERSION:
%All codes tested on Matlab R2021a.
%
%SYNTAX:
%plotpathgraph(model, true)
%plotpathgraph(model, true, 'Radius', 0.25, 'FaceColor', 'r', ...)

%Check bootstrap-input:
if nargin<2
    bootstrap = false;
end

%Set default plot options:
radius = 0.25;
facecolor = [0.75 0.75 0.75];
fontsize = 10;
fontname = 'Century Gothic';
fontcolor = [0 0 0];
plottitle = [];
plotsize = [600 400];
visible = 'on';
xy = [];

%Overwrite default options with given options:
for i=1:2:length(varargin)
    eval([lower(varargin{i}) ' = varargin{i+1};']);
end

%Initialize figure:
figure('Visible', visible);
hold on;

%Set size:
plotsize = [600 400];
resolution = get(0, 'screensize');
set(gcf, 'Position', [(resolution(3)*0.5) - (plotsize(1)*0.5) (resolution(4)*0.5) - (plotsize(2)*0.5) plotsize]);

%Find coordinates for latent variables:
if isempty(xy)
    d = linspace(0, 360, length(model.block_names)+1);
    d = d(1:end-1);
    d = d + 90;
    xy = [cosd(d)' sind(d)'];
end

%Draw paths between the blocks
for t=1:size(model.inner_model_spec, 1)
    for f=1:size(model.inner_model_spec, 2)
        if model.inner_model_spec(t, f) == 1
            
            %Plot line:
            plot(xy([f; t], 1), xy([f; t], 2), 'Color', 'k');
            
            %Plot arrowhead:
            if xy(f, 1)<=xy(t, 1) && xy(f, 2)<=xy(t, 2)
                d = 180 + ([90 180 270] + atand(abs(xy(t, 2)-xy(f, 2)) / abs(xy(t, 1)-xy(f, 1))));
            elseif xy(f, 1)<=xy(t, 1) && xy(f, 2)>xy(t, 2)
                d = 180 - ([90 180 270] + atand(abs(xy(t, 2)-xy(f, 2)) / abs(xy(t, 1)-xy(f, 1))));
            elseif xy(f, 1)>xy(t, 1) && xy(f, 2)<=xy(t, 2)
                d = 0 - ([90 180 270] + atand(abs(xy(t, 2)-xy(f, 2)) / abs(xy(t, 1)-xy(f, 1))));
            elseif xy(f, 1)>xy(t, 1) && xy(f, 2)>xy(t, 2)
                d = 0 + ([90 180 270] + atand(abs(xy(t, 2)-xy(f, 2)) / abs(xy(t, 1)-xy(f, 1))));
            end
            ax = cosd(d) * 0.05 + xy(f, 1)+0.5*(xy(t, 1)-xy(f, 1));
            ay = sind(d) * 0.05 + xy(f, 2)+0.5*(xy(t, 2)-xy(f, 2));
            fill(ax, ay, 'k', 'EdgeColor', 'none');
            
            %Show number:
            tx = cosd(d(1)) * 0.1 + xy(f, 1)+0.5*(xy(t, 1)-xy(f, 1));
            ty = sind(d(1)) * 0.1 + xy(f, 2)+0.5*(xy(t, 2)-xy(f, 2));
            if bootstrap && isfield(model, 'bootstrap')
                text(tx, ty, num2str(round(model.bootstrap.inner_model.mean(t, f), 2)), 'HorizontalAlignment', 'Center', 'VerticalAlignment', 'Middle', 'FontSize', fontsize, 'FontName', fontname, 'FontWeight', 'Bold');
            elseif isfield(model, 'inner_model')
                text(tx, ty, num2str(round(model.inner_model(t, f), 2)), 'HorizontalAlignment', 'Center', 'VerticalAlignment', 'Middle', 'FontSize', fontsize, 'FontName', fontname, 'FontWeight', 'Bold');
            end
        end
    end
end

%Draw blocks for latent variables:
for i=1:length(model.block_names)
    rectangle('Position', [xy(i, 1)-radius, xy(i, 2)-radius, 2*radius, 2*radius], 'Curvature', [1 1], 'EdgeColor', 'k', 'FaceColor', facecolor);
    text(xy(i, 1), xy(i, 2), model.block_names{i}, 'HorizontalAlignment', 'Center', 'VerticalAlignment', 'Middle', 'FontSize', fontsize, 'FontName', fontname, 'FontWeight', 'Bold', 'Color', fontcolor);
end

%Remove axis ticks:
set(gca, 'XTick', []);
set(gca, 'XColor', [1 1 1])
set(gca, 'YTick', []);
set(gca, 'YColor', [1 1 1])

%Set title:
title(plottitle);

end