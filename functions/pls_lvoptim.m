function [B, T, U, P, Q, V, W, signstable, algorithm] = pls_lvoptim(X, Y, Xpp, Ypp, LV, varargin)

%DESCRIPTION:
%A function to perform Partial Least Squares modelling that allows you to
%automatically select the number of latent variables. It acts as a shell
%around the function 'pls.m' by Tim Offermans, which is the core function
%performing the actual PLS regressions. If you also have the function
%'plstrain.m' by Tim Offermans, you might be interested to know that the 
%current function is a trimmed-down version of 'plstrain.m' that does not 
%give you a model-structure back and that cannot be used for discrimant 
%analysis.
%
%INPUT:
%- X: The independent data to model.
%- Y: The dependent data to model.
%- Xpp: The preprocessing method for the independent data (0=none, 1=mean
%  centering, 2=autoscaling).
%- Ypp: The preprocessing method for the dependent data (0=none, 1=mean
%  centering, 2=autoscaling).
%- LV: The number of latent variables to model:
%  - LV = n: Model n latent variables.
%  - LV = Inf: Model maximum number of latent variables.
%  - LV = -n: Optimize number of latent variables between 1 and n, using
%    the validation scheme that can be specified with additional name-value
%    pairs as input arguments. By defailt (whithout this additional input),
%    5-fold Venetian blinds cross-validation is used. You can enter -Inf as
%    input, but this may be unfeasible if there are many dependent
%    variables.
%
%- The following name-value pairs can be used to provide additional input:
%  - 'optscheme': Validation scheme to use for the optimization of the
%    number of LVs:
%    - 'random' = Validation using a single random subset.
%    - 'kfold' = Random cross-validation using k folds.
%    - 'vblindsi' = Venetian blinds cross-validation on the sampling index
%      (recommended for time-series data, this is the default).
%    - 'vblindsy'= Venetian blinds cross-validation on the first column of 
%      Y.
%    - 'leaveout' Leave-one-out cross-validation.
%    - 'cblock' = Contiguous block cross-validation.
%  - 'optn': Validation repeats to use (for non-deterministic schemes, 
%     the default = 1).
%  - 'optk': For cross-validation, this is the number of partitions the 
%    data is divided into (default = 5). For single test set validation, a 
%    fraction of 1/k is taken out of the data for validation (so for k=5, 
%    20% of the data is used for testing.
%  - 'signstable': Whether to use the signstable SVD by Rasmus Bro 
%    (https://doi.org/10.1002/cem.1122) or not (default).
%  - 'algorithm': The algorithm to use for model calculation: 'NIPALS'
%    (default) or 'SIMPLS'.
%
%OUTPUT:
%- B: Regression vector. The first element is B0.
%- T: X-scores.
%- U: Y-scores.
%- P: X-loadings.
%- Q: Y-loadings.
%- V: Fraction of variance explained by each Latent Variable. The columns
%  are the Latent Variables, the first row the variation in X and the second
%  row the variation in Y. For multiple Y variables, this represents the
%  variance of Y in the entire Y-block.
%- W: X-variable weights (sometimes also named R).
%- signstable: Whether the signstable SVD was used or not.
%- algorithm: The algorithm used to calculate this particular model.
%
%AUTHOR:
%Tim Offermans, Radboud University Nijmegen (The Netherlands), October 2021
%
%VERSION:
%All codes tested on Matlab R2021a.
%
%SYNTAX:
%[B, T, U, P, Q, V, W, signstable, algorithm] = pls_lvoptim(X, Y, Xpp, Ypp, LV, varargin)
%[B, T, U, P, Q, V, W, signstable, algorithm] = pls_lvoptim(X, Y, Xpp, Ypp, LV, 'optscheme', 'kfold', 'n', 10, 'k', 5)

%Check and parse input:
optscheme = 'vblindsi';
optn = 1;
optk = 5;
signstable = false;
algorithm = 'NIPALS';
if nargin>5
    for i=1:2:length(varargin)
        eval([varargin{i} ' = varargin{i+1};']);
    end
end
if nargin<5
    LV = Inf;
end
if nargin<4
    Ypp = 1;
end
if nargin<3
    Xpp = 1;
end

%Correct input:
if optk<1
    optk = ceil(1/optk);
end

%Check and correct the number of latent variables requested for modelling:
if abs(LV)>min(size(X))
    LV = sign(LV) * min(size(X));
end
if size(X, 2) == 1
    LV = 1;
end
%Optimize the number of latent variables using (cross-)validation. The
%settings to use for cross-validation can be set by the user, but on
%default a Venetian blinds on the sorted Y-variable is used.
if LV<0
    
    %Determine validation set(s):
    if strcmp(optscheme, 'vblindsi')
        optn = 1;
        test = false(optn, optk, size(Y, 1));
        for i=1:size(test, 2)
            test(1, i, i:size(test, 2):size(test, 3)) = true;
        end
    elseif strcmp(optscheme, 'vblindsy')
        optn = 1;
        test = false(optn, optk, size(Y, 1));
        [~, s] = sort(Y(:, 1));
        for i=1:size(test, 2)
            test(1, i, s(i:size(test, 2):size(test, 3))) = true;
        end
    elseif strcmp(optscheme, 'kfold')
        test = false(optn, optk, size(Y, 1));
        for n = 1:size(test, 1)
            s = randperm(size(test, 3));
            for i=1:size(test, 2)
                test(n, i, s(i:size(test, 2):size(test, 3))) = true;
            end
        end
    elseif strcmp(optscheme, 'random')
        test = false(optn, 1, size(Y, 1));
        for n = 1:size(test, 1)
            s = randperm(size(test, 3));
            test(n, 1, s(1:floor(size(test, 3) * (1/optk)))) = true;
        end
    elseif strcmp(optscheme, 'leaveout')
        optn = 1;
        test = false(optn, optk, size(Y, 1));
        optk = size(Y, 1);
        test = false(1, size(Y, 1), size(Y, 1));
        test(1, find(eye(size(Y, 1)))) = true;
    elseif strcmp(optscheme, 'cblock')
        optn = 1;
        test = false(optn, optk, size(Y, 1));
        for i=1:size(test, 2)
            test(optn, i, floor((i-1) * (size(test, 3) / size(test, 2)) + 1):floor((i) * (size(test, 3) / size(test, 2)))) = true;
        end
	else
        error('Invalid validation scheme. Please check the help-information and your input.');
    end
    
    %Initialize validation output:
    errors = NaN([size(test, 1), abs(LV), size(Y)]);
    Ypcal = NaN([size(test, 1), size(test, 2), abs(LV), size(Y)]);
    Ypval = NaN([size(test, 1), abs(LV), size(Y)]);

    %For each repeat:
    for n = 1:size(test, 1)
    
        %For each fold:
        for k=1:size(test, 2)

            %Divide test and training set:
            Xtrain = X(~test(n, k, :), :);
            Ytrain = Y(~test(n, k, :), :);
            Xtest = X(test(n, k, :), :);
            Ytest = Y(test(n, k, :), :);

            %Preprocess X of train and test set:
            if Xpp>0
                Xtest = Xtest - (ones(size(Xtest, 1), 1) * mean(Xtrain));
                Xtrain = Xtrain - (ones(size(Xtrain, 1), 1) * mean(Xtrain));
            end
            if Xpp>1
                Xtest = Xtest ./ (ones(size(Xtest, 1), 1) * std(Xtrain));
                Xtrain = Xtrain ./ (ones(size(Xtrain, 1), 1) * std(Xtrain));
            end

            %For each latent variable:
            for ilv = 1:abs(LV)

                %Calibrate, test and save performance:
                B = pls(Xtrain, Ytrain, Xpp, Ypp, ilv, signstable, algorithm);
                
                %Calibration performance:
                Yp = (Xtrain * B(2:end, :)) + B(1, :);
                if Ypp>1
                    Yp = Yp .* (ones(size(Yp, 1), 1) * std(Ytrain));
                end
                if Ypp>0
                    Yp = Yp + (ones(size(Yp, 1), 1) * mean(Ytrain));
                end
                Ypcal(n, k, ilv, ~test(n, k, :), :) = Yp;
                
                %Cross-validation performance:
                Yp = (Xtest * B(2:end, :)) + B(1, :);
                if Ypp>1
                    Yp = Yp .* (ones(size(Yp, 1), 1) * std(Ytrain));
                end
                if Ypp>0
                    Yp = Yp + (ones(size(Yp, 1), 1) * mean(Ytrain));
                end
                Ypval(n, ilv, test(n, k, :), :) = Yp;
                errors(n, ilv, test(n, k, :), :) = Ytest - Yp;
            end
        end
    end

    %Select the optimal number of components by first calculating the MSE 
    %for each validation fold and each number of LVs, summing those MSEs
    %over the validation folds (so for each number of LVs), and then
    %selecting the number of LVs that gave the smallest sum of MSEs. This
    %is the same method used in the original Process PLS implementation in
    %R (pathmodelr). If the cross-validation is repeated (which is not an
    %option in pathmodelr), then the minimum MSE over the folds is averaged
    %over the repeats.
    mses = NaN(size(errors, 1), size(test, 2), size(errors, 2));
    for n=1:size(mses, 1)
        for k=1:size(mses, 2)
            for lv=1:abs(LV)
                e = squeeze(errors(1, lv, squeeze(test(n, k, :)), :));
                mses(n, k, lv) = mean(e(:).^2);
            end
        end
    end
    [~, LV] = min(mean(sum(mses, 3), 1));
    
	%The below commented code is another way for selecting the optimal
	%number of components, and was originally programmed for the
	%PLS-software by Tim Offermans. It calculates the RMSE for per number
	%of LVs over all samples (since each sample is used once as test
	%sample), and then selects the number of LVs that gives the smallest
	%RMSE. This is different from the original pathmodelr-implementation,
	%and is kept as commented code for future development as it is
	%suspected to be more robust than the pathmodelr. The pathmodelr-method
	%is however used above to increase similarity between pathmodelr and
	%this Matlab-implementation.
    %errors = squeeze(nanmean(errors.^2, 1).^0.5);
    %errors = squeeze(nanmean(errors(:, :).^2, 2).^0.5);
    %[~, LV] = min(errors);
    
end

%Train (optimized) model:
[B, T, U, P, Q, V, W, signstable, algorithm] = pls(X, Y, Xpp, Ypp, LV, signstable, algorithm);

end