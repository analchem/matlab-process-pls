function Xr = processpls_reverse(model)

%DESCRIPTION:
%This function will reverse an existing Process PLS model to reconstruct
%the measured data from the model. This can help investigating the fit of
%the data by the Process PLS model. It is the equivalent as reversing a PCA
%model, by multiplying the X-scores with the transposed X-loadings to get
%the reconstructed X-data.
%
%For Process PLS such reversion is slightly more complicated, as the model
%is network of PLS models. Reversion is only done for blocks that are
%targets, as only does are predicted. The reversion is done for each target
%block in four steps:
%1. Predict target X/Y-scores from predictor X-scores, using inner model
%   coefficients.
%2. Reverse the scaling procedures done for the X/Y-scores.
%3. Reconstruct measured data from predicted X/Y-scores and X/Y-loadings.
%4. Reverse the mean-centering, autoscaling and block-scaling done for the
%  measured data.
%
%More details are given in the code. The Process PLS model should be
%obtained using 'processpls.m'.
%
%INPUT:
%- model: The Process PLS model, as obtained with 'processpls.m'.
%
%OUTPUT:
%- Xr: The reconstructed measured data matrix for the model. Data for
%  blocks that are not a target will be NaN.
%
%AUTHOR:
%Tim Offermans, Radboud University (The Netherlands), October 2021
%
%VERSION:
%All codes tested on Matlab R2021a.
%
%SYNTAX:
%Xr = processpls_reverse(model)

%Initialize an empty matrix that will contain the reconstructed measured
%data for the entire model:
Xr = NaN(size(model.data));

%Find the reconstructed measured data per block:
for b=1:length(model.blocks)
    
    %If the block is predictor only:
    if ~any(model.inner_model_spec(b, :)) && any(model.inner_model_spec(:, b))
        continue %Do nothing

    %If the block is both predictor and target:
    elseif any(model.inner_model_spec(b, :)) && any(model.inner_model_spec(:, b))
        %1: Reconstruct the outer model X-scores, by multiplying the outer
        %model X-scores of the incoming blocks with the inner model
        %regression coefficient:
        om_t = (model.blocks{b}.im_x * model.blocks{b}.im_b(2:end, :)) + repmat(model.blocks{b}.im_b(1, :), size(model.blocks{b}.im_x, 1), 1);
        
        %2: Revert the variance- and block-scaling that is done for the
        %outer model X-scores, using the scaling factors saved in the
        %model:
        om_t = om_t / diag(model.blocks{b}.om_sfx);
        
        %3: Reconstruct the preprocessed measured data, by multiplying the
        %reconstructed and rescaled outer model X-scores with the 
        %transposed outer model X-loadings:
        Xpp = om_t * model.blocks{b}.om_p';
        
        %4: Revert the preprocessing (mean-centering and autoscaling) of
        %the reconstructed measured data. Because the scaling parameters
        %are not stored in the model, we have to repeat the scaling
        %procedure, save the parameters and then use them to unscale the
        %reconstructed data. This is especially relevant to find the
        %block-scaling factor.
        X = model.blocks{b}.X;
        pp1 = mean(X);
        X = X - (ones(size(X, 1), 1) * pp1);
        pp2 = std(X);
        X = X ./ (ones(size(X, 1), 1) * pp2);
        pp3 = sqrt(mean((X(:).^2) * size(X, 2)));
        X = Xpp;
        X = X .* pp3;
        X = X .* (ones(size(X, 1), 1) * pp2);
        X = X + (ones(size(X, 1), 1) * pp1);
        
        %Save the reconstructed measured data for this block:
        Xr(:, model.outer_model_spec(:, b)) = X;
        
    %If the block is target only
    elseif any(model.inner_model_spec(b, :)) && ~any(model.inner_model_spec(:, b))
        %1: Reconstruct the outer model Y-scores, by multiplying the outer
        %model X-scores of the incoming blocks with the inner model
        %regression coefficients:
        om_u = (model.blocks{b}.im_x * model.blocks{b}.im_b(2:end, :)) + repmat(model.blocks{b}.im_b(1, :), size(model.blocks{b}.im_x, 1), 1);
        
        %2: Revert the variance- and block-scaling that is done for the
        %outer model Y-scores, using the scaling factors saved in the
        %model:
        om_u = om_u / diag(model.blocks{b}.om_sfy);
        
        %3: Reconstruct the preprocessed measured data, by multiplying the
        %reconstructed and rescaled outer model Y-scores with the
        %transposed outer model Y-loadings:
        Xpp = om_u * model.blocks{b}.om_q';
        
        %4: Revert the preprocessing (mean-centering and autoscaling) of
        %the reconstructed measured data. Because the scaling parameters
        %are not stored in the model, we have to repeat the scaling
        %procedure, save the parameters and then use them to unscale the
        %reconstructed data. This is especially relevant to find the
        %block-scaling factor.
        X = model.blocks{b}.X;
        pp1 = mean(X);
        X = X - (ones(size(X, 1), 1) * pp1);
        pp2 = std(X);
        X = X ./ (ones(size(X, 1), 1) * pp2);
        pp3 = sqrt(mean((X(:).^2) * size(X, 2)));
        X = Xpp;
        X = X .* pp3;
        X = X .* (ones(size(X, 1), 1) * pp2);
        X = X + (ones(size(X, 1), 1) * pp1);
        
        %Save the reconstructed measured data for this block:
        Xr(:, model.outer_model_spec(:, b)) = X;
	end
end

end