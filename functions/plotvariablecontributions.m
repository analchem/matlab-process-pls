function plotvariablecontributions(model, varargin)

%DESCRIPTION:
%A function to plot the outer effects for one ore more Process PLS models.
%The function is written to accept the model-structure that is given as
%output by processpls.m. The outer effects are stored such a structure in
%the field 'variable_contributions'. The function accepts single models or a
%structure of multiple models, in case it is desired to compare those.
%
%INPUT:
%- model: A single model to plot the outer effects for, or a structure 
%  of multiple models if multiple models should be plotted for comparison.
%
%- The following name-value pairs can be used:
%  - 'bootstrap': Whether to plot the non-bootstrapped results (''), the
%    bootstrapped mean ('mean') or the bootstrapped standard deviation
%    ('std'), if those are available. This can only be used when the 
%    boostrap-results from 'processpls_boot.m' are present as the field 
%    'bootstrap' in the model-structure.
%  - 'ci': When it is asked to plot the bootstrapped means, the confidence
%    limit to plot using errorbars. Use for instance '95' or '99'. Leave 
%    blank for no confidence limits.
%  - 'absolute': Whether to plot the absolute outer effects (false, 
%    default or not (true).
%  - 'modelnames': Names of the models, to use in the legend.
%  - 'legendposition': Position of the legend.
%  - 'color': An n-by-3 matrix of RGB colors used to distuingish the n
%    models.
%  - 'fontname': Font name for the text (default 'Century Gothic').
%  - 'plottitle': Title of the plot.
%  - 'plotsize': Size of the figure.
%  - 'showgrid': Whether to show the grid or not.
%  - 'subplot': Whether to NOT produce a new figure (but plot in a
%    pre-assigned subplot), or to do produce a new figure.
%  - 'visible': Set the visibility of the plot 'on' (default) or 'off'. 
%    This is useful for scripts were many plots are made and saved to the 
%    disk, but should not clutter the screen. Figures that are not visible 
%    should however still be closed to save memory ('close gcf').
%
%AUTHOR:
%Tim Offermans, Radboud University Nijmegen (The Netherlands), October 2021
%
%SYNTAX:
%plotvariablecontributions(models)
%plotvariablecontributions(models, 'absolute', true, 'boostrap', true, 'ci', 2.576, ...)

%Set default plot options:
bootstrap = false;
ci = 99;
absolute = false;
modelnames = strrep(strcat('Model_', string(num2str((1:length(model))'))), '_', ' ');
legendposition = 'NorthEast';
color = [0.745098039215686,0.192156862745098,0.101960784313725;0.333333333333333,0.745098039215686,0.101960784313725;0.101960784313725,0.654901960784314,0.745098039215686;0.513725490196078,0.101960784313725,0.745098039215686];
fontname = 'Century Gothic';
plottitle = [];
plotsize = [1600 400];
showgrid = 'off';
subplot = false;
visible = 'on';

%Overwrite default options with given options:
for i=1:2:length(varargin)
    eval([lower(varargin{i}) ' = varargin{i+1};']);
end

%If a single model is given, convert it to a structure of 1 model:
if length(model)==1 && isstruct(model)
    models{1} = model;
    model = models;
end

%Initialize figure:
if ~subplot
    figure('Visible', visible);
    resolution = get(0, 'screensize');
    set(gcf, 'Position', [(resolution(3)*0.5) - (plotsize(1)*0.5) (resolution(4)*0.5) - (plotsize(2)*0.5) plotsize]);
end
hold on;

%Plot data:
d = [];
for i=1:length(model)
    if strcmp(bootstrap, 'mean') && isfield(model{1}, 'bootstrap')
        if absolute
            d = [d abs(nansum(model{i}.bootstrap.variable_contributions.mean', 1)')];
        else
            d = [d nansum(model{i}.bootstrap.variable_contributions.mean', 1)'];
        end
    elseif strcmp(bootstrap, 'std') && isfield(model{1}, 'bootstrap')
        d = [d nansum(model{i}.bootstrap.variable_contributions.std', 1)'];
    else
        if absolute
            d = [d abs(nansum(model{i}.variable_contributions', 1)')];
        else
            d = [d nansum(model{i}.variable_contributions', 1)'];
        end
    end
end
b = bar(d);

%Color bars:
for i=1:length(b)
    b(i).FaceColor = color(mod(i-1, size(color, 1))+1, :);
end

%Add legend:
if length(model)>1
    legend(modelnames, 'Location', legendposition, 'AutoUpdate', 'off');
end

%Add and customize axis labels:
set(gca, 'Xlim', [0, size(model{1}.outer_model_spec, 1)+1]);
set(gca, 'XTick', 1:size(model{1}.outer_model_spec, 1));
xnames = string([]);
for v=1:size(model{1}.outer_model_spec, 1)
    xnames = [xnames; [model{1}.block_names{find(model{1}.outer_model_spec(v, :))}, ' V', num2str(sum(model{1}.outer_model_spec(1:v, find(model{1}.outer_model_spec(v, :)))))]];
end
set(gca, 'XTickLabel', xnames);
set(gca, 'XTickLabelRotation', 45);
set(gca, 'FontName', fontname);
if absolute
    ylabel('Absolute outer effect');
else
    ylabel('Outer effect');
end
title(plottitle);

%Add confidence intervals, if requested and possible:
if ~isempty(ci) && strcmp(bootstrap, 'mean') && isfield(model{1}, 'bootstrap')
    ztable = [100,Inf;99,2.57600000000000;98,2.32600000000000;97,2.17000000000000;96,2.05400000000000;95,1.96000000000000;94,1.88100000000000;93,1.81200000000000;92,1.75100000000000;91,1.69500000000000;90,1.64500000000000;89,1.59800000000000;88,1.55500000000000;87,1.51400000000000;86,1.47600000000000;85,1.44000000000000;84,1.40500000000000;83,1.37200000000000;82,1.34100000000000;81,1.31100000000000;80,1.28200000000000;79,1.25400000000000;78,1.22700000000000;77,1.20000000000000;76,1.17500000000000;75,1.15000000000000;74,1.12600000000000;73,1.10300000000000;72,1.08000000000000;71,1.05800000000000;70,1.03600000000000;69,1.01500000000000;68,0.994000000000000;67,0.974000000000000;66.0000000000000,0.954000000000000;65,0.935000000000000;64,0.915000000000000;63,0.896000000000000;62,0.878000000000000;61,0.860000000000000;60,0.842000000000000;59.0000000000000,0.824000000000000;58.0000000000000,0.806000000000000;57.0000000000000,0.789000000000000;56.0000000000000,0.772000000000000;55.0000000000000,0.755000000000000;54,0.739000000000000;53,0.722000000000000;52,0.706000000000000;51,0.690000000000000;50,0.674000000000000;49,0.659000000000000;48,0.643000000000000;47,0.628000000000000;46,0.613000000000000;45.0000000000000,0.598000000000000;44.0000000000000,0.583000000000000;43.0000000000000,0.568000000000000;42.0000000000000,0.553000000000000;41,0.539000000000000;40,0.524000000000000;39,0.510000000000000;38,0.496000000000000;37,0.482000000000000;36,0.468000000000000;35,0.454000000000000;34,0.440000000000000;33.0000000000000,0.426000000000000;32.0000000000000,0.412000000000000;31.0000000000000,0.399000000000000;30.0000000000000,0.385000000000000;29.0000000000000,0.372000000000000;28.0000000000000,0.358000000000000;27,0.345000000000000;26,0.332000000000000;25,0.319000000000000;24,0.305000000000000;23,0.292000000000000;22.0000000000000,0.279000000000000;21.0000000000000,0.266000000000000;20.0000000000000,0.253000000000000;19.0000000000000,0.240000000000000;18.0000000000000,0.228000000000000;17.0000000000000,0.215000000000000;16.0000000000000,0.202000000000000;15.0000000000000,0.189000000000000;14.0000000000000,0.176000000000000;13,0.164000000000000;12,0.151000000000000;11.0000000000000,0.138000000000000;10.0000000000000,0.126000000000000;9.00000000000000,0.113000000000000;8.00000000000000,0.100000000000000;7.00000000000000,0.0880000000000000;6.00000000000001,0.0750000000000000;5.00000000000000,0.0630000000000000;4.00000000000000,0.0500000000000000;3.00000000000000,0.0380000000000000;2.00000000000000,0.0250000000000000;1.00000000000000,0.0130000000000000;0,0];
    [~, z] = min(abs(ci - ztable(:, 1)));
    z = ztable(z, 2);
    for i=1:length(model)
        e = z * (nansum(model{i}.bootstrap.variable_contributions.std', 1)' / sqrt(size(model{i}.bootstrap.bootsamples, 2)));
        if absolute
            errorbar(b(i).XData+b(i).XOffset, abs(nansum(model{i}.bootstrap.variable_contributions.mean', 1)), e, 'LineStyle', 'none', 'Color', 'k');
        else
            errorbar(b(i).XData+b(i).XOffset, nansum(model{i}.bootstrap.variable_contributions.mean', 1), e, 'LineStyle', 'none', 'Color', 'k');
        end
    end    
    legend([string(modelnames); num2str(round(ci, 1)) '% CI{  }'], 'Location', legendposition, 'AutoUpdate', 'off');
end

%Add grid, if requested:
grid(showgrid);

end