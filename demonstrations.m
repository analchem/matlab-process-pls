%% INTRODUCTION:
%This script will perform the same demonstrations in the original Process 
%PLS publication, but using the Process PLS software in Matlab rather than 
%the original implementation in R called 'pathmodelr'. Only demonstration 3
%('Benchmarking') and 5 ('Analysis of traditional multi-block data') are
%performed. Demonstration 4 ('Multi-batch analysis') is not given as the
%data used is confidential. Also, only the Process PLS models are
%calculated and not the PLS-PM models (as this is a different method and
%is not part of this Matlab-software).
%
%The original Process PLS publication can be found at:
%- van Kollenburg, Geert, et al. "Process PLS: Incorporating substantive 
%  knowledge into the predictive modelling of multiblock, multistep, 
%  multidimensional and multicollinear process data." Computers & Chemical 
%  Engineering 154 (2021): 107466.
%
%The data and software files to reproduce the demonstrations 4 and 6 in the 
%paper with R can be found at:
%- https://data.mendeley.com/datasets/9x9h7fr4kn/1
%
%Note that the inner model effects (P^2) for models with multiple latent
%vairables modelled per block obtained here may be (slightly) different
%than those reported in the original Process PLS publication. This is
%because the original implementaiton in R (pathmodelr) uses a random
%10-fold cross-validation to select the optimal number of latent variables
%per block. The model calibration in pathmodelr is thus stochastic, and
%result may be different when the calibration is repeated (also when it is
%repeated with pathmodelr itself). This stochasticity can be dealt with by
%bootstrapping the models. Such bootstrapping is included in this
%demonstration of the Matlab-implementation, but is not included in the
%demonstrations in the original Process PLS publications.
%
%To increase reproducibility of path modelling with Process PLS, this
%Matlab implementation uses Venetian-blinds cross-validation for optimizing
%the number of latent variables. This is a deterministic method, and
%repeating the calibration of a certain model with this Matlab
%implementation should always yield the same results. This is especially
%convenient in a development stage of research. Venetian blinds is however
%just the default method, and can be changed if desired (see help-
%information for 'processpls.m' to learn how).
%
%AUTHOR:
%Tim Offermans, Radboud University Nijmegen (The Netherlands), September 2021
%
%VERSION:
%All codes tested on Matlab R2021a.

%Add folders with functions and data:
clear;
clc;
addpath('functions');
addpath('data');

%% DEMONSTRATION "3. Benchmarking":

%Clear workspace:
clear;
clc;

%Load data from csv. Note that this data has already been corrected for
%time-lags:
data = csvread('Oil Distillation data.csv');

%Specify block names:
block_names = ["Feed"; "Preheater I"; "Preheater II"; "Separation"; "Yield"];

%Specify inner model (which block connects to which block?):
inner_model_spec = false(length(block_names));
inner_model_spec(2, 1) = true; %Feed -> Preheater I
inner_model_spec(5, 1) = true; %Feed -> Yield ratio
inner_model_spec(3, 2) = true; %Preheater I -> Preheater II
inner_model_spec(5, 2) = true; %Preheater I -> Yield ratio
inner_model_spec(4, 3) = true; %Preheater II -> Separation
inner_model_spec(5, 3) = true; %Preheater II -> Yield
inner_model_spec(5, 4) = true; %Separation -> Yield

%Specify outer model (which variable is part of which block?):
outer_model_spec = false(size(data, 2), length(block_names));
outer_model_spec(1:4, 1) = true; %Variables 1-4 are part of Feed
outer_model_spec(5:8, 2) = true; %Variables 5-8 are part of Preheater I
outer_model_spec(9:12, 3) = true; %Variables 9-12 are part of Preheater II
outer_model_spec(13:16, 4) = true; %Variables 13-16 are part of Seperation
outer_model_spec(17, 5) = true; %Variables 17 is part of Yield

%Calculate a Process PLS model that is limited to model a single latent 
%variable per block:
model_1lv = processpls(data, inner_model_spec, outer_model_spec, 'block_names', block_names, 'nlvs', 1);

%Calculate a Process PLS model that is not restrained to the number of
%latent variables to model:
model_wlv = processpls(data, inner_model_spec, outer_model_spec, 'block_names', block_names, 'nlvs', -Inf);

%The inner model effects (P^2, given in the original paper in Table 2) can
%be accessed for both models as following:
model_1lv.inner_model
model_wlv.inner_model

%They can also be plotted in a path-graph with the supplied plotting
%function:
plotpathgraph(model_1lv, false, 'plottitle', 'Process PLS (1 LV / block)')
plotpathgraph(model_wlv, false, 'plottitle', 'Process PLS (W LVs / block)')

%Plotting them as a histogram is also possible with a dedicated plotting
%function:
plotinnermodel(model_1lv, 'plottitle', 'Process PLS (1 LV / block)')
plotinnermodel(model_wlv, 'plottitle', 'Process PLS (W LVs / block)')

%That same function can also be used to plot two models in one figure, for
%easy comparison:
plotinnermodel({model_1lv; model_wlv}, 'modelnames', ["1 LV / block"; "W LVs / block"], 'plottitle', 'Process PLS inner model')

%The explained variances in the outer model (R^2, given in the original
%paper in Table 3) can be accessed for both models as follows:
model_1lv.expvar_in_MV_block
model_wlv.expvar_in_MV_block

%This information can also be conveniently plotted, using the functions 
%below. This function can be used for plotting the results of a single 
%model, but also of that from two models, as is done here:
plotpathexpvar({model_1lv; model_wlv}, 'modelnames', ["1 LV / block"; "W LVs / block"], 'plottitle', 'Process PLS variance explained in measured data blocks')

%We can also plot the contributions of the measured variables in the
%entire model with the following, similar function:
plotvariablecontributions({model_1lv; model_wlv}, 'modelnames', ["1 LV / block"; "W LVs / block"], 'plottitle', 'Process PLS measured variable contributions')

%In the original demonstrations, the models were not bootstrapped. 
%Subjecting the models to boostrapping, in this case 100 repeats, can 
%however be done as follows:
model_1lv.bootstrap = processpls_boot(model_1lv, 100, false, 0.05);
model_wlv.bootstrap = processpls_boot(model_wlv, 100, false, 0.05);

%The bootstrapped mean and confidence interval (in this case 99%) of the
%path effects can be plotted conveniently for both models with the plotting
%function for histograms. Note that the confidence limits can be very 
%narrow. Also, note that the confidence limits may sometimes be below 0 or 
%above 1 eventhough none of the models has a path effect below 0 or above 1 
%(which would be impossible).
plotinnermodel({model_1lv; model_wlv}, 'modelnames', ["1 LV / block"; "W LVs / block"], 'plottitle', 'Process PLS inner model', 'bootstrap', 'mean', 'ci', 99)

%% DEMONSTRATION "5. Analysis of traditional multi-block data":

%Clear workspace:
clear;
clc;

%Load data from csv. Note that this data has already been corrected for
%time-lags:
data = csvread('ValdeLoirData.csv', 1, 1);

%Specify block names:
block_names = ["Smell at rest"; "View"; "Smell after shaking"; "Tasting"; "Global quality"];

%Specify inner model (which block connects to which block?):
inner_model_spec = false(length(block_names));
inner_model_spec(2, 1) = true; %Smell at rest -> View
inner_model_spec(3, 1) = true; %Smell at rest -> Smell after shaking
inner_model_spec(4, 1) = true; %Smell at rest -> Tasting
inner_model_spec(5, 1) = true; %Smell at rest -> Global quality
inner_model_spec(3, 2) = true; %View -> Smell after shaking
inner_model_spec(4, 2) = true; %View -> Tasting
inner_model_spec(5, 2) = true; %View -> Global quality
inner_model_spec(4, 3) = true; %Smell after shaking -> Tasting
inner_model_spec(5, 3) = true; %Smell after shaking -> Global quality
inner_model_spec(5, 4) = true; %Tasting -> Global quality

%Specify outer model (which variable is part of which block?):
outer_model_spec = false(size(data, 2), length(block_names));
outer_model_spec(1:5, 1) = true; %Variables 1-5 are part of Smell at rest
outer_model_spec(6:8, 2) = true; %Variables 6-8 are part of View
outer_model_spec(9:18, 3) = true; %Variables 9-18 are part of Smell after shaking
outer_model_spec(19:27, 4) = true; %Variables 19-27 are part of Tasting
outer_model_spec(28, 5) = true; %Variables 28 is part of Global quality

%In the paper, only a Process PLS model is reported for which the maximum 
%number of latent variables to model was not restrained. This model can be
%obtained as follows:
model_wlv = processpls(data, inner_model_spec, outer_model_spec, 'block_names', block_names, 'nlvs', -Inf);

%The inner model effects (P^2, given in the original paper in Table 7) can
%be accessed for this model as:
model_wlv.inner_model

%Although not reported in the original Process PLS paper and demonstration,
%one could be interested in calculating a Process PLS model with just a
%single latent variable for each block. This can be done with the below
%code. The equivalent code in the original pathmodelr implementation will
%in this case yield the exact same results, as the number of latent
%variables to model is not optimized and the model calculation is thus
%always deterministic.
model_1lv = processpls(data, inner_model_spec, outer_model_spec, 'block_names', block_names, 'nlvs', 1);
model_1lv.inner_model

%For both models, the inner path effects (P^2 values) can also be plotted 
%in a path-graph with the supplied plotting function:
plotpathgraph(model_1lv, false, 'plottitle', 'Process PLS (1 LV / block)')
plotpathgraph(model_wlv, false, 'plottitle', 'Process PLS (W LVs / block)')

%Plotting them as a histogram is also possible with a dedicated plotting
%function:
plotinnermodel(model_1lv, 'plottitle', 'Process PLS (1 LV / block)')
plotinnermodel(model_wlv, 'plottitle', 'Process PLS (W LVs / block)')

%That same function can also be used to plot two models in one figure, for
%easy comparison:
plotinnermodel({model_1lv; model_wlv}, 'modelnames', ["1 LV / block"; "W LVs / block"], 'plottitle', 'Process PLS inner model')

%In the original paper and demonstration, bootstrapping is not used. 
%Subjecting the models to boostrapping, in this case 100 repeats, can 
%however be done as follows:
model_1lv.bootstrap = processpls_boot(model_1lv, 100, false, 0.05);
model_wlv.bootstrap = processpls_boot(model_wlv, 100, false, 0.05);

%The bootstrapped mean and confidence interval (in this case 99%) of the
%path effects can be plotted conveniently for both models with the plotting
%function for histograms. Note that the confidence limits can be very 
%narrow. Also, note that the confidence limits may sometimes be below 0 or 
%above 1 eventhough none of the models has a path effect below 0 or above 1 
%(which would be impossible).
plotinnermodel({model_1lv; model_wlv}, 'modelnames', ["1 LV / block"; "W LVs / block"], 'plottitle', 'Process PLS inner model', 'bootstrap', 'mean', 'ci', 99);